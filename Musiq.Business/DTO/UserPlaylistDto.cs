﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.DTO
{
  public  class UserPlaylistDto
  {
        public int UserPlaylistId { get; set; }
        public int UserId { get; set; }
        public int PlaylistId { get; set; }
        public System.DateTime PlayedDate { get; set; }
        public bool Finished { get; set; }
        public bool Playing { get; set; }

        public int SortNumber { get; set; }
    }
}
