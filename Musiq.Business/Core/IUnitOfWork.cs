﻿using Musiq.Business.Core.Repositoties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Core
{
    public interface IUnitOfWork
    {
        IUpdateRepository UpdateRepository { get; }
        IUserPlaylistItemRepository UserPlaylistItemRepository { get; }
        IUserPlaylistRepository UserPlaylistRepository { get; }
        IDeviceRepository DeviceRepository { get; }
        ICategoryRepository CategoryRepository { get; }
        IUserRepository UserRepository { get; }
        IForgotPasswordRepository ForgotPasswordRepository { get; }
        IFrequentlyAskedQuestionRepository FrequentlyAskedQuestionRepository { get; }
        IUserStatusRepository UserStatusRepository { get; }
        IUserTypeRepository UserTypeRepository { get; }
        IProductRepository ProductRepository { get; }
        IProductStatusRepository ProductStatusRepository { get; }
        ITransactionRepository TransactionRepository { get; }
        ITransactionStatusRepository TransactionStatusRepository { get; }
        IPlaylistRepository PlaylistRepository { get; }
        IPlayListItemRepository PlayListItemRepository { get; }
        IDashboardRepository DashboardRepository { get; }
        Task<int> Complete();
    }
}
