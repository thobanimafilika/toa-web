﻿using Musiq.Business.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Core.Repositoties
{
   public interface ICategoryRepository : IRepository<Category>
    {
    }
}
