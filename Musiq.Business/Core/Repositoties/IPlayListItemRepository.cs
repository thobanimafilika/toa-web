﻿using Musiq.Business.DB;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Core.Repositoties
{
   public interface IPlayListItemRepository : IRepository<PlayListItem>
    {
        Task<List<PlayListItemProductDto>> GetListByPlayListId(int playListId);
        Task<List<PlayListItemDto>> GetList();
    }
}
