﻿using Musiq.Business.DB;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Core.Repositoties
{
   public interface IUserRepository : IRepository<User>
    {
        Task<User> Login(string email, string password);
        Task<User> Register(string email, string password, string firstName, string lastname, string mobileNumber);
        Task<bool> UserExist(string email, bool removeNoteActive);
        Task<bool> ChangePassword(string email, string currentPassword, string newPasword);
        Task<User> Saver(UserDto userDto);
    }
}
