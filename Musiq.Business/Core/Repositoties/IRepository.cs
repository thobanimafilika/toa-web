﻿using Musiq.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Core.Repositoties
{
     public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> Get(int id);
        Task<IEnumerable<TEntity>> GetAll();
        PageList<TEntity> Find(UserParam userParam, params Expression<Func<TEntity, object>>[] includes);
        PageList<TEntity> Find(Expression<Func<TEntity, bool>> predicate, UserParam userParam);
        PageList<TEntity> FindWithInclude(Expression<Func<TEntity, bool>> predicate, UserParam userParam, params Expression<Func<TEntity, object>>[] includes);
        IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate,  params Expression<Func<TEntity, object>>[] includes);
        Task<TEntity> SingleOrDefault(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes);
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        Task<int> Update(TEntity entity);
    }
}
