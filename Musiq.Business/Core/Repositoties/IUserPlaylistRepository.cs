﻿using Musiq.Business.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Core.Repositoties
{
 
    public interface IUserPlaylistRepository : IRepository<UserPlaylist>
    {
        Task<int> NextPlayList(int categoryId, int userId);
        Task<int> NextUserPlayList(int categoryId, int userId);
        UserPlaylist LastUserPlayList(int playlistId, int userId);
    }
}
