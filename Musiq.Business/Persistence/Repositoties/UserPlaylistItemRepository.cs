﻿using Musiq.Business.Core.Repositoties;
using Musiq.Business.DB;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Persistence.Repositoties
{
      public class UserPlaylistItemRepository : Repository<UserPlaylistItem>, IUserPlaylistItemRepository
    {
        Entities _context;
        public UserPlaylistItemRepository(Entities context)
           : base(context)
        {
            _context = context;
        }


      async public Task<List<UserPlaylistItemDto>> GetEnabledPlaylist(int userId, int playlistId)
        {
            var results = from userPlayListItem in _context.UserPlaylistItems
                          join playlistItem in _context.PlayListItems on userPlayListItem.PlaylistItemId equals playlistItem.PlayListItemId
                          where playlistItem.PlaylistId == playlistId && userPlayListItem.UserId == userId
                          && playlistItem.IsDeleted == false
                          && userPlayListItem.IsDeleted == false
                          select new UserPlaylistItemDto
                          {
                              Enabled = userPlayListItem.Enabled,
                              EnabledDate = userPlayListItem.EnabledDate,
                              UserId = userPlayListItem.UserId,
                              PlaylistItemId = userPlayListItem.PlaylistItemId
                          };

            return await results.ToListAsync();
        }
    }
}
