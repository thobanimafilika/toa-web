﻿using Musiq.Business.Core.Repositoties;
using Musiq.Business.DB;
using Musiq.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Persistence.Repositoties
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseModel
    {
        protected readonly Entities Context;

        public Repository(Entities context)
        {
            Context = context;
        }

        public Repository()
        {
        }

        public IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes)
        {
            var data = Context.Set<TEntity>().Where(predicate).Where(c => c.IsDeleted == false).ToList();
            var queryIncludes = includes
                        .Aggregate(
                            data.AsQueryable(),
                            (current, include) => current.Include(include)
                        );

            return queryIncludes;
        }
        async public Task<TEntity> Get(int id)
        {
            
            return await Context.Set<TEntity>().FindAsync(id);
        }

        async public Task<IEnumerable<TEntity>> GetAll()
        {
            var data = Context.Set<TEntity>().Where(c=>!c.IsDeleted);
            return await data.ToListAsync();
        }

         public PageList<TEntity> Find(UserParam userParam, params Expression<Func<TEntity, object>>[] includes)
        {
            var data = Context.Set<TEntity>().Where(c => !c.IsDeleted);
            if (includes != null)
            {
                var queryIncludes = includes
                        .Aggregate(
                            data.AsQueryable(),
                            (current, include) => current.Include(include)
                        );

                var pageList = PageList<TEntity>.CreateAsync(queryIncludes.ToList(), userParam.PageNumber, userParam.PageSize);
                return pageList;
            }
            else
            {
                var pageList = PageList<TEntity>.CreateAsync(data.ToList(), userParam.PageNumber, userParam.PageSize);
                return pageList;
            }
        }

         public PageList<TEntity> Find(Expression<Func<TEntity, bool>> predicate, UserParam userParam)
        {
            var data = Context.Set<TEntity>().Where(predicate);
            data = data.Where(c => !c.IsDeleted);
            var pageList = PageList<TEntity>.CreateAsync(data.ToList(), userParam.PageNumber, userParam.PageSize);
            return pageList;
        }

        async public Task<TEntity> SingleOrDefault(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includes)
        {
            var query = Context.Set<TEntity>().Where(predicate);
            query = query.Where(c => !c.IsDeleted);
            if (includes != null)
            {
                var queryIncludes = includes
                        .Aggregate(
                            query.AsQueryable(),
                            (current, include) => current.Include(include)
                        );

                return await queryIncludes.FirstOrDefaultAsync();
            }
            else
            {
                return await query.FirstOrDefaultAsync();
            }
        }

         public void Add(TEntity entity)
        {
             Context.Set<TEntity>().Add(entity);
        }

        async public Task<int> Update(TEntity entity)
        {
            this.Context.Entry(entity).State = EntityState.Modified;
            return await Context.SaveChangesAsync();
        }

         public void AddRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().AddRange(entities);
        }

         public PageList<TEntity> FindWithInclude(Expression<Func<TEntity, bool>> predicate, UserParam userParam, params Expression<Func<TEntity, object>>[] includes)
        {
            var query = Context.Set<TEntity>().Where(predicate);
            query = query.Where(c => !c.IsDeleted);
            if (includes != null)
            {
                var queryIncludes = includes
                        .Aggregate(
                            query.AsQueryable(),
                            (current, include) => current.Include(include)
                        );
                var pageList = PageList<TEntity>.CreateAsync(queryIncludes.ToList(), userParam.PageNumber, userParam.PageSize);
                return pageList;
            }
            else
            {
                var pageList = PageList<TEntity>.CreateAsync(query.ToList(), userParam.PageNumber, userParam.PageSize);
                return pageList;
            }
        }
    }
}
