﻿using Musiq.Business.Core.Repositoties;
using Musiq.Business.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Persistence.Repositoties
{
    
    public class DashboardRepository :  IDashboardRepository
    {
        Entities _context;
        public DashboardRepository(Entities context)
        {
            _context = context;
        }
        
        async public Task<object> GetAdminDashboard()
        {
    
        var query = (
                           from productListItem in _context.UserStatus
                           //join product in _context.Products on productListItem.ProductId equals product.ProductId
                           // join category in _context.Categories on product.CategoryId equals category.CategoryId
                           // where  category.IsDeleted == false
                           where //product.IsDeleted == false
                           productListItem.IsDeleted == false
                           select new 
                           {
                               User = _context.Users.Count(c => c.IsDeleted == false && c.UserStatus.Code == "act"),
                               Product = _context.Products.Count(c => c.IsDeleted == false) ,
                               Playlist = _context.PlayLists.Count(c => c.IsDeleted == false),
                               Transanction = _context.Transactions.Count(c => c.IsDeleted == false),
                               Faq = _context.FrequentlyAskedQuestions.Count(c => c.IsDeleted == false)
                           }
                         );
            var list = await Task.Run(() => query.FirstOrDefault());
            return list;
        }
    }
}
