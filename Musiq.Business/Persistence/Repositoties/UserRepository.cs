﻿using Musiq.Business.Core.Repositoties;
using Musiq.Business.DB;
using Musiq.Common;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Persistence.Repositoties
{
  
    public class UserRepository : Repository<User>, IUserRepository
    {
        Entities _context;
        public UserRepository(Entities context)
           : base(context)
        {
            _context = context;
        }


        async public Task<User> Login(string email, string password)
        {
            var user = await _context.Users.FirstOrDefaultAsync(c => c.EmailAddress == email && !c.IsDeleted);
            if (user == null)
            {
                return null;
            }

            if (!VerifyPasswordHas(password, user.PasswordHash, user.PasswordSalt))
                return null;

            return user;
        }

        private bool VerifyPasswordHas(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedPasswordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedPasswordHash.Length; i++)
                {
                    if (passwordHash[i] != computedPasswordHash[i])
                        return false;
                }
            }

            return true;
        }

        async public Task<User> Saver(UserDto userDto)
        {
            
            User user = new User();
            bool update = false;
            if (userDto.UserId > 0)
            {
                user = _context.Users.FirstOrDefault(c => c.UserId == userDto.UserId);
                if(user != null)
                {
                    update = true;
                }
            }
            user.FirstName = userDto.FirstName;
            user.LastName = userDto.LastName;
            user.CreatedBy = 1;
            user.CreatedDate = DateTime.Now;
            user.UserTypeId = userDto.UserTypeId;
            user.UserStatusId = userDto.UserStatusId;
            user.Mobile = userDto.Mobile;
            user.EmailAddress = userDto.EmailAddress;
            user.Gender = userDto.Gender;
            user.ModifiedDate = DateTime.Now;
            user.ModifiedBy = userDto.ModifiedBy;
           
            user.VerificationCode = new Random().Next(1111, 9999).ToString();
            //Create password salt and hash
            if (!update)
            {
                byte[] passwordHash, passwordSalt;
                Utilities.CreatePasswordHash(userDto.Password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;

                //Save user to the database
                _context.Users.Add(user);
            }
            else
            {
                _context.Entry(user).State = EntityState.Modified;
            }
            await _context.SaveChangesAsync();

            return user;
        }

        async public Task<User> Register(string email, string password, string firstName, string lastname, string mobileNumber)
        {
            UserStatus userStatus = await _context.UserStatus.FirstOrDefaultAsync(c => c.Code == "PEN");
            UserType userType = await _context.UserTypes.FirstOrDefaultAsync(c => c.Code == "CUS");
            User user = new User();
            user.FirstName = firstName;
            user.LastName = lastname;
            user.CreatedBy = 1;
            user.CreatedDate = DateTime.Now;
            user.UserTypeId = userType.UserTypeId;
            user.UserStatusId = userStatus.UserStatusId;
            user.EmailAddress = email;
            user.ActiveTill = DateTime.Now.AddDays(-1);
            user.Mobile = mobileNumber;
            user.VerificationCode = new Random().Next(1111, 9999).ToString();
            //Create password salt and hash
            byte[] passwordHash, passwordSalt;
            Utilities.CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            //Save user to the database
            _context.Users.Add(user);
            await _context.SaveChangesAsync();

            return user;
        }

       

        public async Task<bool> ChangePassword(string email, string currentPassword, string newPasword)
        {
            var user = await _context.Users.FirstOrDefaultAsync(c => c.EmailAddress == email && !c.IsDeleted);
            if (user == null)
                return false;

            if (!VerifyPasswordHas(currentPassword, user.PasswordHash, user.PasswordSalt))
                return false;

            byte[] passwordHash, passwordSalt;
            Utilities.CreatePasswordHash(newPasword, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return true;

        }
        async public Task<bool> UserExist(string email, bool removeNoteActive)
        {
            var user = await _context.Users.FirstOrDefaultAsync(c => c.EmailAddress == email && !c.IsDeleted);
            if(user != null)
            {
                var status = await _context.UserStatus.FirstOrDefaultAsync(c=>c.UserStatusId ==user.UserStatusId);
                if (status != null && status.Code.ToUpper() == "ACT")
                {
                    return true;
                }
                else
                {
                    if(removeNoteActive)
                    {
                        user.IsDeleted = true;
                        _context.Entry(user).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }

            return false;
            // return await _context.Users.AnyAsync(c => c.EmailAddress == email && !c.IsDeleted);
        }
    }
}
