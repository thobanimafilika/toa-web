﻿using Musiq.Business.Core.Repositoties;
using Musiq.Business.DB;
using Musiq.Business.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Persistence.Repositoties
{

    public class UserPlaylistRepository : Repository<UserPlaylist>, IUserPlaylistRepository
    {
        Entities _context;
        public UserPlaylistRepository(Entities context)
           : base(context)
        {
            _context = context;
        }

        /// <summary>
        /// This is version one of getting next playlist
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
       async public Task<int> NextPlayList(int categoryId, int userId)
        {

            var previousPlayList = (from userPlayList in _context.UserPlaylists
                                    join playList in _context.PlayLists on userPlayList.PlaylistId equals playList.PlayListId
                                    where userPlayList.UserId == userId
                                    && playList.CategoryId == categoryId
                                    && userPlayList.IsDeleted == false
                                    && playList.IsDeleted ==false
                                    orderby userPlayList.UserPlaylistId descending
                                    select new UserPlaylistDto
                                    {
                                        Finished = userPlayList.Finished,
                                        PlayedDate = userPlayList.PlayedDate,
                                        PlaylistId = userPlayList.PlaylistId,
                                        UserId = userPlayList.UserId,
                                        UserPlaylistId = userPlayList.UserPlaylistId,
                                        Playing = userPlayList.Playing

                                    }).FirstOrDefault();
            
           
            if (previousPlayList == null)
            {
                return await GetFirstPlaylist(categoryId, userId);
            }
            else if(previousPlayList.Finished)
            {
                return await GetNextPlaylist(categoryId, userId, previousPlayList.SortNumber);
            }
            else
            {
                //if (TotalHours(previousPlayList.PlayedDate, DateTime.Now) > 6)
                //{
                //    var previousPlayListItem = _context.UserPlaylists.FirstOrDefault(c => c.UserPlaylistId == previousPlayList.UserPlaylistId);
                //    if (previousPlayListItem != null)
                //    {
                //        previousPlayListItem.Finished = true;
                //        await Update(previousPlayListItem);
                //    }
                //    return await GetNextPlaylist(categoryId, userId, previousPlayList.PlaylistId);
                //}
                //else
                   return previousPlayList.PlaylistId;
            }

          
        }

        async public Task<int> NextUserPlayList(int categoryId, int userId)
        {

            var previousPlayList = (from userPlayList in _context.UserPlaylists
                                    join playList in _context.PlayLists on userPlayList.PlaylistId equals playList.PlayListId
                                    where userPlayList.UserId == userId
                                    && playList.CategoryId == categoryId
                                    && userPlayList.IsDeleted == false
                                    && playList.IsDeleted == false
                                    orderby userPlayList.UserPlaylistId descending
                                    select new UserPlaylistDto
                                    {
                                        Finished = userPlayList.Finished,
                                        PlayedDate = userPlayList.PlayedDate,
                                        PlaylistId = userPlayList.PlaylistId,
                                        UserId = userPlayList.UserId,
                                        UserPlaylistId = userPlayList.UserPlaylistId,
                                        Playing = userPlayList.Playing,
                                        SortNumber = playList.SortNumber.Value

                                    }).FirstOrDefault();


            if (previousPlayList == null)
            {
                return await GetFirstPlaylist(categoryId, userId);
            }
            else if (previousPlayList.Finished)
            {
                return await GetNextPlaylist(categoryId, userId, previousPlayList.SortNumber);
            }
            else
            {
                if (TotalHours(previousPlayList.PlayedDate, DateTime.Now) > 8 && previousPlayList.Playing)
                {
                    var previousPlayListItem = _context.UserPlaylists.FirstOrDefault(c => c.UserPlaylistId == previousPlayList.UserPlaylistId);
                    if (previousPlayListItem != null)
                    {
                        previousPlayListItem.Finished = true;
                        await Update(previousPlayListItem);
                    }
                    return await GetNextPlaylist(categoryId, userId, previousPlayList.SortNumber);
                }
                else
                    return previousPlayList.PlaylistId;
            }


        }



        double TotalHours(DateTime start, DateTime endDate)
        {
            TimeSpan ts = endDate - start;

            return ts.TotalHours;
        }


        async Task<int> GetNextPlaylist(int categoryId, int userId, int sortNumber)
        {
            var playlist = _context.PlayLists.OrderBy(c => c.SortNumber).FirstOrDefault(c => c.CategoryId == categoryId && c.SortNumber > sortNumber && c.IsDeleted == false && !c.IsOptional);
            if (playlist == null)
                return await GetFirstPlaylist(categoryId, userId);
            else
            {
                await CreateUserPlaylist(userId, playlist.PlayListId);
                return playlist.PlayListId;
            }
        }
        async Task<int> GetFirstPlaylist(int categoryId, int userId)
        {
            var playlist = _context.PlayLists.OrderBy(c=>c.SortNumber).FirstOrDefault(c => c.CategoryId == categoryId && c.IsDeleted == false && !c.IsOptional);
            if (playlist != null)
            {
                await CreateUserPlaylist(userId, playlist.PlayListId);
                return playlist.PlayListId;
            }
            return 0;
        }

      async  Task<bool> CreateUserPlaylist(int userId, int playListId)
        {
            var userPlayList = new UserPlaylist();
            userPlayList.CreatedBy = userId;
            userPlayList.CreatedDate = DateTime.Now;
            userPlayList.IsDeleted = false;
            userPlayList.ModifiedBy = userId;
            userPlayList.PlayedDate = DateTime.Now;
            userPlayList.UserId = userId;
            userPlayList.PlaylistId = playListId;
            userPlayList.Finished = false;
            _context.Set<UserPlaylist>().Add(userPlayList);
            int saved = await _context.SaveChangesAsync();
            return saved > 0;


        }

        public UserPlaylist LastUserPlayList(int playlistId, int userId)
        {
            var userPlaylist =  _context.UserPlaylists.OrderByDescending(c => c.UserPlaylistId).FirstOrDefault(c => c.PlaylistId == playlistId && c.UserId == userId && !c.IsDeleted);
            return userPlaylist;
        }
    }
}
