﻿using Musiq.Business.Core.Repositoties;
using Musiq.Business.DB;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Persistence.Repositoties
{

    public class PlayListItemRepository : Repository<PlayListItem>, IPlayListItemRepository
    {
        private Entities _context;
        public PlayListItemRepository(Entities context)
           : base(context)
        {
            _context = context;
        }

        public Task<List<PlayListItemDto>> GetList()
        {
            var query = from list in _context.PlayListItems
                        join product in _context.Products on list.ProductId equals product.ProductId
                        where list.IsDeleted == false
                        && product.IsDeleted == false
                        select new PlayListItemDto
                        {
                            Title = product.Title,
                            Description = product.Description,
                            PlaylistId = list.PlaylistId,
                            PlayListItemId = list.PlayListItemId,
                            ProductId = list.ProductId

                        };

            return query.ToListAsync();

        }

        async public Task<List<PlayListItemProductDto>> GetListByPlayListId(int playListId)
        {
            var query = (
                           from productListItem in _context.PlayListItems
                           join product in _context.Products on productListItem.ProductId equals product.ProductId
                           join playlist in _context.PlayLists on productListItem.PlaylistId equals playlist.PlayListId
                           join category in _context.Categories on playlist.CategoryId equals category.CategoryId
                           where productListItem.PlaylistId == playListId
                           && category.IsDeleted == false
                           && product.IsDeleted == false
                           && playlist.IsDeleted == false
                           && productListItem.IsDeleted == false
                           select new PlayListItemProductDto
                           {
                               ProductId = product.ProductId,
                               ProductDescription = product.Description,
                               ProductLink = product.Link,
                               ProductCategoryId = playlist.CategoryId,
                               ProductCategoryDescription = category.Description,
                               ProductTitle = product.Title,
                               PlayListItemId = productListItem.PlayListItemId,
                               PlaylistId = productListItem.PlaylistId,
                               PlayListItemDescription = productListItem.Description,
                               PlayListItemTitle = productListItem.Title
                           }
                         );
            var list = await Task.Run(() => query.ToList());
            return list;
        }
    }
}
