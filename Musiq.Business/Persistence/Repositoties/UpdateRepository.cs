﻿using Musiq.Business.Core.Repositoties;
using Musiq.Business.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Persistence.Repositoties
{
   
    public class UpdateRepository : Repository<Update>, IUpdateRepository
    {
        public UpdateRepository(Entities context)
           : base(context)
        {
        }
    }
}
