﻿using Musiq.Business.Core.Repositoties;
using Musiq.Business.DB;

namespace Musiq.Business.Persistence.Repositoties
{
    public class UserTypeRepository : Repository<UserType>, IUserTypeRepository
    {
        public UserTypeRepository(Entities context)
           : base(context)
        {
        }
    }
   
}
