﻿using Musiq.Business.Core;
using Musiq.Business.Core.Repositoties;
using Musiq.Business.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Business.Persistence.Repositoties
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly Entities _context;

        public UnitOfWork(Entities context)
        {
            _context = context;
            UpdateRepository = new UpdateRepository(_context);
            DeviceRepository = new DeviceRepository(_context);
            TransactionRepository = new TransactionRepository(_context);
            DashboardRepository = new DashboardRepository(_context);
            UserRepository = new UserRepository(_context);
            UserStatusRepository = new UserStatusRepository(_context);
            UserTypeRepository = new UserTypeRepository(_context);
            CategoryRepository = new CategoryRepository(_context);
            ProductRepository = new ProductRepository(_context);
            ProductStatusRepository = new ProductStatusRepository(_context);
            TransactionStatusRepository = new TransactionStatusRepository(_context);
            PlaylistRepository = new PlaylistRepository(_context);
            PlayListItemRepository = new PlayListItemRepository(_context);
            ForgotPasswordRepository = new ForgotPasswordRepository(_context);
            FrequentlyAskedQuestionRepository = new FrequentlyAskedQuestionRepository(_context);
            UserPlaylistRepository = new UserPlaylistRepository(_context);
            UserPlaylistItemRepository = new UserPlaylistItemRepository(_context);
        }
        public IUpdateRepository UpdateRepository { get; private set; }

        public IUserPlaylistItemRepository UserPlaylistItemRepository  { get; private set; }
        public IUserPlaylistRepository UserPlaylistRepository { get; private set; }
        public IDeviceRepository DeviceRepository { get; private set; }

        public IDashboardRepository DashboardRepository { get; private set; }
        public ITransactionRepository TransactionRepository { get; private set; }
        public IUserRepository UserRepository { get; private set; }
        public IUserStatusRepository UserStatusRepository { get; private set; }
        public IUserTypeRepository UserTypeRepository { get; private set; }

        public ICategoryRepository CategoryRepository { get; private set; }
        public IProductRepository ProductRepository { get; private set; }
        public IProductStatusRepository ProductStatusRepository { get; private set; }
        public ITransactionStatusRepository TransactionStatusRepository { get; private set; }
        public IPlaylistRepository PlaylistRepository { get; private set; }
        public IPlayListItemRepository PlayListItemRepository { get; private set; }

        public IForgotPasswordRepository ForgotPasswordRepository { get; private set; }
        public IFrequentlyAskedQuestionRepository FrequentlyAskedQuestionRepository { get; private set; }

        async public Task<int> Complete()
        {
            return await _context.SaveChangesAsync();

        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
