USE [master]
GO
/****** Object:  Database [Musiq]    Script Date: 2019/08/05 16:20:05 ******/
CREATE DATABASE [Musiq]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Musiq', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Musiq.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Musiq_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Musiq_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Musiq] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Musiq].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Musiq] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Musiq] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Musiq] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Musiq] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Musiq] SET ARITHABORT OFF 
GO
ALTER DATABASE [Musiq] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Musiq] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Musiq] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Musiq] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Musiq] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Musiq] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Musiq] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Musiq] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Musiq] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Musiq] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Musiq] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Musiq] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Musiq] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Musiq] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Musiq] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Musiq] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Musiq] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Musiq] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Musiq] SET  MULTI_USER 
GO
ALTER DATABASE [Musiq] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Musiq] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Musiq] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Musiq] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Musiq] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Musiq]
GO
/****** Object:  User [admin]    Script Date: 2019/08/05 16:20:05 ******/
CREATE USER [admin] FOR LOGIN [admin] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[tCategory]    Script Date: 2019/08/05 16:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCategory](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_tCategory] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tComment]    Script Date: 2019/08/05 16:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tComment](
	[CommentId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[Email] [varchar](50) NULL,
	[PostId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_tComment] PRIMARY KEY CLUSTERED 
(
	[CommentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tPost]    Script Date: 2019/08/05 16:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tPost](
	[PostId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_tPost] PRIMARY KEY CLUSTERED 
(
	[PostId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tProduct]    Script Date: 2019/08/05 16:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tProduct](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](max) NOT NULL,
	[Link] [varchar](max) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[ProductStatusId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[CategoryId] [int] NOT NULL,
	[Title] [varchar](150) NOT NULL,
 CONSTRAINT [PK_tProduct] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tProductStatus]    Script Date: 2019/08/05 16:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tProductStatus](
	[ProductStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_tProductStatus] PRIMARY KEY CLUSTERED 
(
	[ProductStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tTransaction]    Script Date: 2019/08/05 16:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tTransaction](
	[TransactionId] [int] IDENTITY(1,1) NOT NULL,
	[Amount] [float] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[TransactionStatusId] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[Guid] [varchar](max) NOT NULL,
 CONSTRAINT [PK_tTransaction] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tTransactionStatus]    Script Date: 2019/08/05 16:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tTransactionStatus](
	[TransactionStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_tTransactionStatus] PRIMARY KEY CLUSTERED 
(
	[TransactionStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tUser]    Script Date: 2019/08/05 16:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tUser](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
	[ActiveTill] [datetime] NULL,
	[EmailAddress] [varchar](50) NOT NULL,
	[PasswordHash] [varbinary](max) NOT NULL,
	[Mobile] [varchar](50) NULL,
	[Gender] [nchar](10) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
	[UserStatusId] [int] NOT NULL,
	[UserTypeId] [int] NOT NULL,
	[PasswordSalt] [varbinary](max) NOT NULL,
	[VerificationCode] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tUser] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tUserStatus]    Script Date: 2019/08/05 16:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tUserStatus](
	[UserStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_tUserStatus] PRIMARY KEY CLUSTERED 
(
	[UserStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tUserType]    Script Date: 2019/08/05 16:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tUserType](
	[UserTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](20) NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_tUserType] PRIMARY KEY CLUSTERED 
(
	[UserTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tTransaction] ON 

INSERT [dbo].[tTransaction] ([TransactionId], [Amount], [IsDeleted], [CreatedBy], [TransactionStatusId], [CreatedDate], [ModifiedDate], [ModifiedBy], [Guid]) VALUES (1, 50, 0, 9, 1, CAST(N'2019-08-04T22:41:16.970' AS DateTime), NULL, NULL, N'PAYID-LVDUFVQ991421307U357264T')
INSERT [dbo].[tTransaction] ([TransactionId], [Amount], [IsDeleted], [CreatedBy], [TransactionStatusId], [CreatedDate], [ModifiedDate], [ModifiedBy], [Guid]) VALUES (2, 50, 0, 9, 1, CAST(N'2019-08-04T22:45:20.100' AS DateTime), NULL, NULL, N'PAYID-LVDUHXQ29B43305UK049804U')
INSERT [dbo].[tTransaction] ([TransactionId], [Amount], [IsDeleted], [CreatedBy], [TransactionStatusId], [CreatedDate], [ModifiedDate], [ModifiedBy], [Guid]) VALUES (3, 50, 0, 9, 1, CAST(N'2019-08-04T23:04:32.820' AS DateTime), NULL, NULL, N'PAYID-LVDUQWY2UT564877B389031M')
INSERT [dbo].[tTransaction] ([TransactionId], [Amount], [IsDeleted], [CreatedBy], [TransactionStatusId], [CreatedDate], [ModifiedDate], [ModifiedBy], [Guid]) VALUES (4, 50, 0, 9, 1, CAST(N'2019-08-04T23:14:07.217' AS DateTime), NULL, NULL, N'PAYID-LVDUVHQ8EL572539V588860E')
INSERT [dbo].[tTransaction] ([TransactionId], [Amount], [IsDeleted], [CreatedBy], [TransactionStatusId], [CreatedDate], [ModifiedDate], [ModifiedBy], [Guid]) VALUES (5, 50, 0, 9, 2, CAST(N'2019-08-04T23:14:07.217' AS DateTime), NULL, NULL, N'PAYID-LVDUVHQ8EL572539V588860E')
INSERT [dbo].[tTransaction] ([TransactionId], [Amount], [IsDeleted], [CreatedBy], [TransactionStatusId], [CreatedDate], [ModifiedDate], [ModifiedBy], [Guid]) VALUES (6, 50, 0, 9, 2, CAST(N'2019-08-04T23:14:07.217' AS DateTime), NULL, NULL, N'PAYID-LVDUVHQ8EL572539V588860E')
INSERT [dbo].[tTransaction] ([TransactionId], [Amount], [IsDeleted], [CreatedBy], [TransactionStatusId], [CreatedDate], [ModifiedDate], [ModifiedBy], [Guid]) VALUES (7, 50, 0, 9, 2, CAST(N'2019-08-04T23:14:07.217' AS DateTime), NULL, NULL, N'PAYID-LVDUVHQ8EL572539V588860E')
INSERT [dbo].[tTransaction] ([TransactionId], [Amount], [IsDeleted], [CreatedBy], [TransactionStatusId], [CreatedDate], [ModifiedDate], [ModifiedBy], [Guid]) VALUES (8, 50, 0, 9, 2, CAST(N'2019-08-04T23:20:55.643' AS DateTime), NULL, NULL, N'PAYID-LVDUYNQ915728642L216724N')
INSERT [dbo].[tTransaction] ([TransactionId], [Amount], [IsDeleted], [CreatedBy], [TransactionStatusId], [CreatedDate], [ModifiedDate], [ModifiedBy], [Guid]) VALUES (9, 50, 0, 10, 2, CAST(N'2019-08-04T23:30:56.470' AS DateTime), NULL, NULL, N'PAYID-LVDU5DY1RG123761M336174N')
SET IDENTITY_INSERT [dbo].[tTransaction] OFF
SET IDENTITY_INSERT [dbo].[tTransactionStatus] ON 

INSERT [dbo].[tTransactionStatus] ([TransactionStatusId], [Code], [Description], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (1, N'PEN', N'Pending', 0, 1, CAST(N'2019-08-04T22:04:15.263' AS DateTime), NULL, NULL)
INSERT [dbo].[tTransactionStatus] ([TransactionStatusId], [Code], [Description], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (2, N'APP', N'Approved', 0, 1, CAST(N'2019-08-04T22:04:25.730' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tTransactionStatus] OFF
SET IDENTITY_INSERT [dbo].[tUser] ON 

INSERT [dbo].[tUser] ([UserId], [FirstName], [LastName], [ActiveTill], [EmailAddress], [PasswordHash], [Mobile], [Gender], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedDate], [ModifiedBy], [UserStatusId], [UserTypeId], [PasswordSalt], [VerificationCode]) VALUES (1, N'Geekay', N'Ncube', NULL, N'geekay@gmail.com', 0x347F94BDB31BE97460F940C0447B9AF9EF5A697078DB0386D3C69A150CDA70240628FB0942797F58E2518B89AA18FDB1B7252058DF8FC4BD5173CD6B7F57386B, N'01984585785', N'Male      ', 0, 1, CAST(N'2019-08-05T14:49:34.157' AS DateTime), CAST(N'2019-08-05T14:49:34.157' AS DateTime), 1, 2, 1, 0xC68B0A213DEB78EB044D1EAB0D06F7B4DB6CB1D1D4B8BF66871A511412E0F940A87F422C9DDF0E5CFD11E56CADFA84865929EA138BBCEF56434A7E9846E92A10EF892CD0BC151646188DB440E48613BEFC48A5F7510C67A246EB7ED5DA9A0C93134BC09EE6C1E1BAA2F4F458102337556AC3211DE1928D2E7DDF42E18DA857F3, N'6856')
SET IDENTITY_INSERT [dbo].[tUser] OFF
SET IDENTITY_INSERT [dbo].[tUserStatus] ON 

INSERT [dbo].[tUserStatus] ([UserStatusId], [Code], [Description], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (1, N'DEC', N'Deactivated', 0, 1, CAST(N'2019-08-05T00:01:16.580' AS DateTime), NULL, NULL)
INSERT [dbo].[tUserStatus] ([UserStatusId], [Code], [Description], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (2, N'ACT', N'Active', 0, 1, CAST(N'2019-08-05T00:01:44.473' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tUserStatus] OFF
SET IDENTITY_INSERT [dbo].[tUserType] ON 

INSERT [dbo].[tUserType] ([UserTypeId], [Code], [Description], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (1, N'ADM', N'Admin', 0, 1, CAST(N'2019-08-04T23:59:35.253' AS DateTime), NULL, NULL)
INSERT [dbo].[tUserType] ([UserTypeId], [Code], [Description], [IsDeleted], [CreatedBy], [CreatedDate], [ModifiedDate], [ModifiedBy]) VALUES (2, N'CUS', N'Customer', 0, 1, CAST(N'2019-08-04T23:59:46.073' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[tUserType] OFF
ALTER TABLE [dbo].[tCategory] ADD  CONSTRAINT [DF_Category_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tCategory] ADD  CONSTRAINT [DF_Category_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tComment] ADD  CONSTRAINT [DF_Comment_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tComment] ADD  CONSTRAINT [DF_Comment_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tPost] ADD  CONSTRAINT [DF_Post_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tPost] ADD  CONSTRAINT [DF_Post_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tProduct] ADD  CONSTRAINT [DF_tProduct_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tProduct] ADD  CONSTRAINT [DF_tProduct_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tProductStatus] ADD  CONSTRAINT [DF_ProductStatus_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tProductStatus] ADD  CONSTRAINT [DF_ProductStatus_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tTransaction] ADD  CONSTRAINT [DF_tTransaction_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tTransaction] ADD  CONSTRAINT [DF_tTransaction_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tTransactionStatus] ADD  CONSTRAINT [DF_TransactionStatus_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tTransactionStatus] ADD  CONSTRAINT [DF_TransactionStatus_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tUser] ADD  CONSTRAINT [DF_tUser_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tUser] ADD  CONSTRAINT [DF_tUser_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tUserStatus] ADD  CONSTRAINT [DF_tUserStatus_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tUserStatus] ADD  CONSTRAINT [DF_tUserStatus_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tUserType] ADD  CONSTRAINT [DF_tUserType_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[tUserType] ADD  CONSTRAINT [DF_tUserType_CreatedDate]  DEFAULT (getdate()) FOR [CreatedDate]
GO
ALTER TABLE [dbo].[tComment]  WITH CHECK ADD  CONSTRAINT [FK_tComment_tPost] FOREIGN KEY([PostId])
REFERENCES [dbo].[tPost] ([PostId])
GO
ALTER TABLE [dbo].[tComment] CHECK CONSTRAINT [FK_tComment_tPost]
GO
ALTER TABLE [dbo].[tProduct]  WITH CHECK ADD  CONSTRAINT [FK_tProduct_tCategory] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[tCategory] ([CategoryId])
GO
ALTER TABLE [dbo].[tProduct] CHECK CONSTRAINT [FK_tProduct_tCategory]
GO
ALTER TABLE [dbo].[tProduct]  WITH CHECK ADD  CONSTRAINT [FK_tProduct_tProduct] FOREIGN KEY([ProductStatusId])
REFERENCES [dbo].[tProductStatus] ([ProductStatusId])
GO
ALTER TABLE [dbo].[tProduct] CHECK CONSTRAINT [FK_tProduct_tProduct]
GO
ALTER TABLE [dbo].[tTransaction]  WITH CHECK ADD  CONSTRAINT [FK_tTransaction_tTransaction] FOREIGN KEY([TransactionId])
REFERENCES [dbo].[tTransaction] ([TransactionId])
GO
ALTER TABLE [dbo].[tTransaction] CHECK CONSTRAINT [FK_tTransaction_tTransaction]
GO
ALTER TABLE [dbo].[tTransaction]  WITH CHECK ADD  CONSTRAINT [FK_tTransaction_tTransactionStatus] FOREIGN KEY([TransactionStatusId])
REFERENCES [dbo].[tTransactionStatus] ([TransactionStatusId])
GO
ALTER TABLE [dbo].[tTransaction] CHECK CONSTRAINT [FK_tTransaction_tTransactionStatus]
GO
ALTER TABLE [dbo].[tUser]  WITH CHECK ADD  CONSTRAINT [FK_tUser_tUserStatus] FOREIGN KEY([UserStatusId])
REFERENCES [dbo].[tUserStatus] ([UserStatusId])
GO
ALTER TABLE [dbo].[tUser] CHECK CONSTRAINT [FK_tUser_tUserStatus]
GO
ALTER TABLE [dbo].[tUser]  WITH CHECK ADD  CONSTRAINT [FK_tUser_tUserType] FOREIGN KEY([UserTypeId])
REFERENCES [dbo].[tUserType] ([UserTypeId])
GO
ALTER TABLE [dbo].[tUser] CHECK CONSTRAINT [FK_tUser_tUserType]
GO
USE [master]
GO
ALTER DATABASE [Musiq] SET  READ_WRITE 
GO
