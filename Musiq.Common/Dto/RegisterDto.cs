﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Musiq.Common.Dto
{
    public class RegisterDto
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email is required")]
        public string EmailAddress { get; set; }
        public string Mobile { get; set; }
        public string MobileNumber { get; set; }// used when registerring only
        public string Gender { get; set; }

        [Required]
        [StringLength(15, MinimumLength = 6, ErrorMessage = "Password must be between 6 and 15 charactors")]
        public string Password { get; set; }
    }
}
