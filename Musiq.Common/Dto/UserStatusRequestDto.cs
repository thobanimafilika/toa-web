﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
    public class UserStatusRequestDto
    {
        public int UserStatusId { get; set; }
        public int UserId { get; set; } 
    }
}
