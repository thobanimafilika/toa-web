﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
    public class ContactUsDto
    {
        public string  Name { get; set; }
        public string  Email { get; set; }
        public string  Title { get; set; }
        public string  Body { get; set; }
        public string  Phone { get; set; }
        public string  HowDidYouHearAboutUs { get; set; }
    }
}
