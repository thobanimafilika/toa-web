﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{

    public class PlayListItemProductDto
    {
        public int ProductId { get; set; } 
        public string ProductDescription { get; set; } 
        public string ProductLink { get; set; } 
        public int ProductCategoryId { get; set; } 
        public string ProductCategoryDescription { get; set; } 
        public string ProductTitle { get; set; } 
        public int PlayListItemId { get; set; } 
        public int PlaylistId { get; set; }
        public string PlayListItemDescription { get; set; }
        public string PlayListItemTitle { get; set; }
    }
}
