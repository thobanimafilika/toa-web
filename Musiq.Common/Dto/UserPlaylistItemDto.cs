﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
   public class UserPlaylistItemDto
    {
        [Required]
        public int UserId { get; set; }
        [Required]
        public int PlaylistItemId { get; set; }
        [Required]
        public System.DateTime EnabledDate { get; set; }
        [Required]
        public bool Enabled { get; set; }
    }
}
