﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
   public class TransactionStatusDto
    {
        public int TransactionStatusId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
