﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
    public class ProductDto
    {
        public int ProductId { get; set; }
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Link is required")]
        public string Link { get; set; }
        public int ProductStatusId { get; set; }
        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }
        public string Icon { get; set; }
        public int Hour { get; set; }
        public int Minute { get; set; }
        public int Second { get; set; }
        public int CreatedBy { get; set; }

        public string Code { get; set; }

    }
}
