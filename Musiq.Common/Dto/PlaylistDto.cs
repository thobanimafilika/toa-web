﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
  public  class PlaylistDto
    { 
        public int PlayListId { get; set; } 
        public string Banner { get; set; } 
        public string Title { get; set; } 
        public string SubTitle { get; set; } 
        public string Description { get; set; }
        public bool IsOptional { get; set; }        
        public int CategoryId { get; set; }
        public string Code { get; set; }
        public int SortNumber { get; set; }

    }
}
