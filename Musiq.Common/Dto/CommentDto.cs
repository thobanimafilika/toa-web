﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
   public class CommentDto
    {
        public int CommentId { get; set; }
        [Required(ErrorMessage = "Description is required")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }

        public string Email { get; set; }
        public int CreatedBy { get; set; }
        public int PostId { get; set; }
    }
}
