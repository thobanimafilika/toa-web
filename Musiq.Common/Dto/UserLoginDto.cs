﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
    public class UserLoginDto
    {
        [Required]
        public string EmailAddress { get; set; }

        [Required]
        public string Password { get; set; }

        public string DeviceOs { get; set; }
        public string SerialNumber { get; set; }
        public string Description { get; set; }

    }
}
