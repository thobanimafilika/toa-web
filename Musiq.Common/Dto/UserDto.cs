﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
  public  class UserDto
    {
        public int UserId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public System.DateTime? ActiveTill { get; set; }
        [Required]
        public string EmailAddress { get; set; }
        public string Mobile { get; set; }
        public string Gender { get; set; }
        [Required]
        public int UserStatusId { get; set; }
        [Required]
        public int UserTypeId { get; set; }
        [Required]
        public string Password { get; set; }
        public string VerificationCode { get; set; }
        public string Token { get; set; }
        public int ModifiedBy { get; set; }
        public UserStatusDto UserStatus { get; set; }
        public string UserTypeCode { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
