﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
  public  class TransactionDto
    {
        public int TransactionId { get; set; }
        public double Amount { get; set; }
        public int TransactionStatusId { get; set; }
        public string Guid { get; set; }
        public TransactionStatusDto TransactionStatus { get; set; }
    }
}
