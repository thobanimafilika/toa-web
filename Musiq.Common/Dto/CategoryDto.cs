﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
  public  class CategoryDto
    {
        public int CategoryId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
