﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
    

    public class UserVerifyDto
    {

        [Required]
        public string EmailAddress { get; set; }

        [Required]
        public string VerificationCode { get; set; }
    }
}
