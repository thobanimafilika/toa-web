﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
   public class PostDto
    {
        public int PostId { get; set; }
        [Required(ErrorMessage = "Description")]

        public string Description { get; set; }
        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }
        public List<CommentDto> Comments { get; set; }
        public int CreatedBy { get; set; }
    }
}
