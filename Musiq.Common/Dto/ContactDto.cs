﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
  public  class ContactDto
    {
        public string EmailAddress { get; set; }
        public string Message { get; set; }

    }
}
