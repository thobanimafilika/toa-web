﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
  public  class UserPlaylistUpdateDto
    {
        [Required]
        public int PlaylistId { get; set; }
        [Required]
        public int UserId { get; set; }
    }
}
