﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common.Dto
{
   public class PlayListItemDto
    {
        public int PlayListItemId { get; set; } 
        public int PlaylistId { get; set; } 
        public int ProductId { get; set; } 
        public string Description { get; set; } 
        public string Title { get; set; }
        public int CategoryId { get; set; }
    }
}
