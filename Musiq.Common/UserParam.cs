﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Musiq.Common
{
    public class UserParam
    {
        private const int MaxPageSize = 100;
        public int PageNumber { get; set; } = 1;
        public string Search { get; set; } = "";
        private int pageSize = 50;
        public int PageSize { get { return pageSize; } set { pageSize = (value > MaxPageSize) ? MaxPageSize : value; } }
    }
}
