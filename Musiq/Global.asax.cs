﻿using Musiq.Business.DB;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Musiq
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            AutoMapper.Mapper.Initialize(cfg => {
                cfg.CreateMap<User, UserDto>();
                cfg.CreateMap<UserDto, User>();

                cfg.CreateMap<UserStatus, UserStatusDto>();
                cfg.CreateMap<UserStatusDto, UserStatus>();

                cfg.CreateMap<Category, CategoryDto>();
                cfg.CreateMap<CategoryDto, Category>();

                //cfg.CreateMap<Comment, CommentDto>();
                //cfg.CreateMap<CommentDto, Comment>();

                //cfg.CreateMap<Post, PostDto>();
                //cfg.CreateMap<PostDto, Post>();

                cfg.CreateMap<Product, ProductDto>();
                cfg.CreateMap<ProductDto, Product>();

                cfg.CreateMap<Transaction, TransactionDto>();
                cfg.CreateMap<TransactionDto, Transaction>();

                cfg.CreateMap<TransactionStatus, TransactionStatusDto>();
                cfg.CreateMap<TransactionStatusDto, TransactionStatus>();

                cfg.CreateMap<UserType, UserTypeDto>();
                cfg.CreateMap<UserTypeDto, UserType>();

                cfg.CreateMap<PlayList, PlaylistDto>();
                cfg.CreateMap<PlaylistDto, PlayList>();

                cfg.CreateMap<PlayListItem, PlayListItemDto>();
                cfg.CreateMap<PlayListItemDto, PlayListItem>();

                cfg.ValidateInlineMaps = false;

            });
        }
    }
}
