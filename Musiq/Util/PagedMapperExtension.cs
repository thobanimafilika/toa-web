﻿using AutoMapper;
using Musiq.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Musiq.Util
{
    public static class PagedMapperExtension
    {
        public static object ToPagedMapper<Tnew, TOrginal>(this PageList<TOrginal> list)
        {
            var data = Mapper.Map<List<Tnew>>(list);
            return new
            {
                CurrentPage = list.CurrentPage,
                TotalPages = list.TotalPages,
                PageSize = list.PageSize,
                TotalCount = (int)Math.Ceiling(list.TotalPages * (double)list.PageSize),
                Data = data ?? new List<Tnew>()
            };
        }
    }
}