﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Musiq.Util
{
    public class CloudinaryHelper
    {
        private static string CloudName = System.Configuration.ConfigurationManager.AppSettings["CloudinaryName"].ToString();
        private static string ApiKey = System.Configuration.ConfigurationManager.AppSettings["CloudinaryApiKey"].ToString();
        private static string AuthEmail = System.Configuration.ConfigurationManager.AppSettings["CloudinaryEmail"].ToString();
        private static string AuthPassword = System.Configuration.ConfigurationManager.AppSettings["CloudinaryEmailPassword"].ToString();
        private static string ApiSecret = System.Configuration.ConfigurationManager.AppSettings["CloudinaryApiSecret"].ToString();


        public static string Upload(Stream biytes, string ImageName)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var stream = biytes;

            Account acc = new Account(
             CloudName,
              ApiKey,
              ApiSecret);

            Cloudinary m_cloudinary = new Cloudinary(acc);

            var uploadParams = new ImageUploadParams()
            {
                File = new FileDescription(ImageName, stream)
            };
            var uploadResult = m_cloudinary.Upload(uploadParams);

            return uploadResult.Uri.ToString();
        }
    }
}