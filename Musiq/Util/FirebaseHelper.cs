﻿using Firebase.Auth;
using Firebase.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace Musiq.Util
{
    public class FirebaseHelper
    {
        private static string FireBaseBucketUrl = System.Configuration.ConfigurationManager.AppSettings["FireBaseBucketUrl"].ToString();
        private static string FireBaseBucketFolder = System.Configuration.ConfigurationManager.AppSettings["FireBaseBucketFolder"].ToString();
        private static string FirebaseKey = System.Configuration.ConfigurationManager.AppSettings["FirebaseKey"].ToString();
        private static string FireBaseMyGmail = System.Configuration.ConfigurationManager.AppSettings["FireBaseMyGmail"].ToString();
        private static string FireBaseMyGmailPassword = System.Configuration.ConfigurationManager.AppSettings["FireBaseMyGmailPassword"].ToString();


        public static async Task<string> UploadAsync(Stream biytes, string ImageName)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var stream = biytes;

            try
            {
                var auth = new FirebaseAuthProvider(new FirebaseConfig(FirebaseKey));
                var a = await auth.SignInWithEmailAndPasswordAsync(FireBaseMyGmail, FireBaseMyGmailPassword);

                var task = new FirebaseStorage(FireBaseBucketUrl, new FirebaseStorageOptions
                {
                    AuthTokenAsyncFactory = () => Task.FromResult(a.FirebaseToken),
                    ThrowOnCancel = true
                })
                .Child(ImageName)
                .PutAsync(stream);

                var downloadUrl = await task;
                return downloadUrl;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return "";
        }
    }
}