﻿using AutoMapper;
using Musiq.App_Start;
using Musiq.Business.Core;
using Musiq.Business.DB;
using Musiq.Common;
using Musiq.Common.Dto;
using Musiq.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Musiq.Controllers
{

    [RoutePrefix("api/[controller]"), JwtAuthorize]
    //[RoutePrefix("api/[controller]")]
    public class PlaylistController : BaseController
    {
        IUnitOfWork _unitOfWork;
        public PlaylistController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet()]
        public HttpResponseMessage SyncPlaylist()
        {
            try
            {
                var result = _unitOfWork.PlaylistRepository.GetList(c => !c.IsDeleted);

                if (result != null)
                {
                    result = result.OrderBy(c => c.SortNumber);
                    var items = Mapper.Map<List<PlaylistDto>>(result);
                    return OkResult(items);
                }

                return Bad("No data");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpGet()]
        public HttpResponseMessage GetList()
        {
            try
            {
                var result = _unitOfWork.PlaylistRepository.FindWithInclude(c => !c.IsDeleted, new UserParam(), inc => inc.PlayListItems);

                if (result != null)
                {
                    var results = result.ToList().OrderBy(c => c.SortNumber.Value);
                    var items = Mapper.Map<List<PlaylistDto>>(results);
                    return OkResult(items);
                }

                return Bad("No data");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }


        [HttpPost()]
        public async Task<HttpResponseMessage> Save([System.Web.Http.FromBody]PlaylistDto playlistDto)
        {
            try
            {
                if (playlistDto == null)
                    return Bad("All fieds are required");

                var playlist = Mapper.Map<PlayList>(playlistDto);

                if (playlist.PlayListId == 0)
                {
                    playlist.CreatedDate = DateTime.Now;
                    playlist.CreatedBy = GetUserId();
                }
                else
                {
                    playlist.ModifiedDate = DateTime.Now;
                    playlist.ModifiedBy = GetUserId();
                }

                if (string.IsNullOrEmpty(playlist.Banner))
                {
                    playlist.Banner = "TOA_DEFAULT_01.png";
                }
                if (string.IsNullOrEmpty(playlist.Title))
                {
                    return Bad("All fieds are required");
                }
                if (string.IsNullOrEmpty(playlist.SubTitle))
                {
                    return Bad("All fieds are required");
                }
                if (string.IsNullOrEmpty(playlist.Description))
                {
                    return Bad("All fieds are required");
                }

      
                if (playlist.PlayListId <= 0)
                {
                    _unitOfWork.PlaylistRepository.Add(playlist);
                }
                else
                {
                    await _unitOfWork.PlaylistRepository.Update(playlist);
                }
              
                 await _unitOfWork.Complete();
                if (playlist?.PlayListId > 0)
                    return OkResult(playlist?.PlayListId ?? -1);
                else
                    return Bad("An error occured, please try again");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }


        [ActionName("SaveImageBanner")]
        [HttpPost]
        public async Task<HttpResponseMessage> SaveImageBanner()
        {
            HttpResponseMessage response = Request.CreateResponse(System.Net.HttpStatusCode.Created);
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {


                var httpRequest = HttpContext.Current.Request;

                var postStringPlaylist = httpRequest.Form["playlistId"];
                var uploadType = httpRequest.Form["cloudProvider"];

                if (!int.TryParse(postStringPlaylist, out int playlistId))
                {

                    return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, "Playlist id does not exist");

                }

                var result = await _unitOfWork.PlaylistRepository.SingleOrDefault(c => c.PlayListId == playlistId);
                if (result == null)
                {
                    return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, "Playlist id not found");
                }


                foreach (string file in httpRequest.Files)
                {


                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {
                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");
                            dict.Add("error", message);
                            return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 1 mb.");

                            dict.Add("error", message);
                            return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {


                            var name = "TOA_BANNER_" + Guid.NewGuid() + extension;

                            var fileUrl = "";

                            if (uploadType == "firebase")
                            {
                                fileUrl = await FirebaseHelper.UploadAsync(postedFile.InputStream, name);
                            }
                            else
                            {
                                fileUrl = CloudinaryHelper.Upload(postedFile.InputStream, name);
                            }


                            result.Banner = fileUrl;

                            if (!string.IsNullOrEmpty(fileUrl))
                            {
                                await _unitOfWork.PlaylistRepository.Update(result);
                                return OkResult(result.Banner);
                            }
                            else
                            {
                                return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest, "Failed uploading image");
                            }
                        }
                    }
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound, dict);
            }
            catch (Exception ex)
            {

                dict.Add("error", LogException(ex));
                return Request.CreateResponse(System.Net.HttpStatusCode.NotFound, dict);
            }
        }

        string LogException(Exception e)
        {
            var messages = new List<string>();
            do
            {
                messages.Add(e.Message);
                e = e.InnerException;
            }
            while (e != null);

            return string.Join(" - ", messages);

        }

        [HttpGet()]
        public async Task<HttpResponseMessage> GetPlaylistImage(int playlistId = 0)
        {
            var playlist = await _unitOfWork.PlaylistRepository.Get(playlistId);

            if (!System.IO.Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/Img/")))
            {
                System.IO.Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/Img/"));
            }
            String filePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Img/" + playlist?.Banner);

            if (!System.IO.File.Exists(filePath))
            {
                filePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Img/default.png");
            }

            var result = new HttpResponseMessage(System.Net.HttpStatusCode.OK);

            System.IO.FileStream fileStream = System.IO.File.OpenRead(filePath);  // new System.IO.FileStream(filePath, FileMode.Open);
            System.Drawing.Image image = System.Drawing.Image.FromStream(fileStream);
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            image.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            result.Content = new ByteArrayContent(memoryStream.ToArray());
            result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/jpeg");
            return result;
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> GetPlayList(int playlistId)
        {
            try
            {

                var result = await _unitOfWork.PlaylistRepository.SingleOrDefault(c => c.PlayListId == playlistId);
                if (result != null)
                {
                    var product = Mapper.Map<PlaylistDto>(result);
                    return OkResult(product);
                }
                return Bad("playlist doesn't exists");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> Delete(int playListId = -1)
        {
            try
            {
                var result = await _unitOfWork.PlaylistRepository.SingleOrDefault(c => c.PlayListId == playListId);
                if (result != null)
                {
                    var getPlayListItems = _unitOfWork.PlayListItemRepository.GetList(c => c.PlaylistId == playListId);

                    if (getPlayListItems == null || getPlayListItems?.Count() <= 0)
                    {
                        result.IsDeleted = true;
                        var product = await _unitOfWork.PlaylistRepository.Update(result);
                        await _unitOfWork.Complete();
                        return OkResult("Playlist deleted successfully");
                    }
                    return Bad("playlist has dependant items and can not be removed");
                }
                return Bad("playlist doesn't exists");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again" + ex.Message);
            }
        }

        [HttpGet()]
        public HttpResponseMessage Search(int pageNumber, int pageSize, string q)
        {
            try
            {
                var result = new PageList<PlayList>(new List<PlayList>(), 1, 1, 10);

                if (!string.IsNullOrEmpty(q))
                {
                    result = _unitOfWork.PlaylistRepository.Find(c => c.Title.ToLower().Contains(string.IsNullOrEmpty(q) ? "" : q.ToLower()), new UserParam() { PageNumber = pageNumber, PageSize = pageSize, Search = string.IsNullOrEmpty(q) ? "" : q.ToLower() });

                }
                else
                {
                    result = _unitOfWork.PlaylistRepository.Find(c => c.IsDeleted == false, new UserParam() { PageNumber = pageNumber, PageSize = pageSize });
                }

                if (result != null)
                {
                    return OkResult(result.ToPagedMapper<PlaylistDto, PlayList>());
                }

                return Bad("No data");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }
    }


}