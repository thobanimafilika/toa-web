﻿using Musiq.App_Start;
using Musiq.Business.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Musiq.Controllers
{
    [RoutePrefix("api/[controller]"), JwtAuthorize]

    //[RoutePrefix("api/[controller]")]

    public class UpdateController : BaseController
    {
        IUnitOfWork _unitOfWork;
        public UpdateController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet()]
      async  public Task<HttpResponseMessage> GetUpdate()
        {
            try
            {

                var result = await _unitOfWork.UpdateRepository.SingleOrDefault(c => !c.IsDeleted);
                if (result != null)
                {
                    return OkResult(new
                    {
                        Code = "APP",
                        Message  = result.UpdateGuid
                    });
                }
                return Bad("no results");

            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }


        [HttpGet()]
        async public Task<HttpResponseMessage> Update()
        {
            try
            {

                var result = await _unitOfWork.UpdateRepository.SingleOrDefault(c => !c.IsDeleted);
                if (result == null)
                {
                    result = new Business.DB.Update();
                    result.CreatedBy = 1;
                    result.CreatedDate = DateTime.Now;
                }

                result.UpdateGuid = Guid.NewGuid().ToString();
                result.ModifiedBy = 1;
                result.ModifiedDate = DateTime.Now;
                int Id = 0;
                if (result.UpdateId == 0)
                {
                    _unitOfWork.UpdateRepository.Add(result);
                    Id = await _unitOfWork.Complete();
                }
                else
                {
                    Id = await _unitOfWork.UpdateRepository.Update(result);
                }
                if (Id > 0)
                {
                    return OkResult("Updates saved");
                }

                return Bad("Updates could not be saved, please try again");

            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }
    }
}