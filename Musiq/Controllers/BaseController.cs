﻿using Musiq.Business.Core;
using Musiq.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Musiq.Controllers
{
    public class BaseController  : ApiController
    {
        protected UserParam GetUserParam(int pageNumber, string search = "")
        {
            UserParam userParam = new UserParam();
            if (pageNumber == 0)
                pageNumber = 1;
            userParam.PageNumber = pageNumber;
            if (search == null)
                search = "";
            userParam.Search = search;

            return userParam;

        }

        protected HttpResponseMessage OkResult(string obj)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
            httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            
            httpResponseMessage.Content = new StringContent(obj, Encoding.UTF8, "application/json");
            return httpResponseMessage;
        }

        protected HttpResponseMessage OkResult(object obj)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
            httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            string json = "";
            if (obj != null)
            {
                json = JsonConvert.SerializeObject(obj);
            }
            httpResponseMessage.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return httpResponseMessage;
        }

        protected HttpResponseMessage Bad(string message)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
            httpResponseMessage = Request.CreateResponse(HttpStatusCode.BadRequest);
            string json = "";

             var result = new
                {
                 Code = "DEC",
                 Message = message
             };

                json = JsonConvert.SerializeObject(result);
            
            httpResponseMessage.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return httpResponseMessage;
        }

        protected HttpResponseMessage BadRequest(string message)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();            
            httpResponseMessage = Request.CreateResponse(HttpStatusCode.BadRequest);
            string json = "";
            if(message != null)
            {
                var result = new
                {
                    Code = "DEC",
                    Message = message
                };
                json = JsonConvert.SerializeObject(result);
            }
            httpResponseMessage.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return httpResponseMessage;
        }

        protected HttpResponseMessage Bad(string message, string code)
        {
            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
            httpResponseMessage = Request.CreateResponse(HttpStatusCode.BadRequest);
            string json = "";
            if (message != null)
            {
                var result = new
                {
                    Code = code,
                    Message = message
                };
                json = JsonConvert.SerializeObject(result);
            }
            httpResponseMessage.Content = new StringContent(json, Encoding.UTF8, "application/json");
            return httpResponseMessage;
        }

        protected string GetClaim(string claimName)
        {
            var value = "";

            var identity = (System.Security.Claims.ClaimsIdentity)User.Identity;
            if (identity != null)
            {
                IEnumerable<System.Security.Claims.Claim> claims = identity.Claims;
                if (claims != null)
                {

                    value = identity.FindFirst(claimName)?.Value;
                }
            }
            return value;
        }


        public int GetUserId()
        {
            int.TryParse(GetClaim("userid"), out int userid);
            return userid;
        }

      protected async Task<Tuple<bool, string>> Authorised(IUnitOfWork _unitOfWork)
        {
            var createdBy = GetClaim("UserName");
            var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.EmailAddress == createdBy, b => b.UserType, e => e.UserStatus);
            if (user == null)
                return new Tuple<bool,string>(false,"User doesnt exists");

            if (user.UserStatus.Code.ToLower() != "act")
                return new Tuple<bool, string>(false, "User doesnt deactivated");

            if (user.UserType.Code.ToUpper() != "ADM")
            {
                if (user.UserType.Code.ToLower() == "cus" && DateTime.Now > user.ActiveTill)
                    return new Tuple<bool, string>(false, "Not authorised to view this");
            }
            return new Tuple<bool, string>(true, "");
        }
    }
}