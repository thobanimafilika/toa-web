﻿using AutoMapper;
using Microsoft.IdentityModel.Tokens;
using Musiq.App_Start;
using Musiq.Business.Core;
using Musiq.Business.DB;
using Musiq.Common;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Musiq.Util;

namespace Musiq.Controllers
{
    [RoutePrefix("api/[controller]")]
    public class UserController : BaseController
    {
        IUnitOfWork _unitOfWork;
        public UserController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> test()
        {
            //StringBuilder error = new StringBuilder();
          //bool  sent = Utilities.SendEmail(new List<string>() { "geekay.ncube@gmail.com" }, "Internet Email", "test", "geekay.ncube@gmail.com", ref error);

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("<center><br/>");
            stringBuilder.Append("Hi " + "Geekay " + "<br/><br/>");
            stringBuilder.Append("Your new password is " + 85858 + "<br/><br/>");
            stringBuilder.Append("Please consider changing it your profile.<br/><br/>");
            stringBuilder.Append("If you have any quiries, please contact us on the email below.<br/><br/>");
            stringBuilder.Append("</center><br/><br/>");
            StringBuilder error = new StringBuilder("");
            bool sent = Utilities.SendEmail(new List<string>() { "geekay.ncube@gmail.com" }, "Password", stringBuilder.ToString(), "", ref error);


            return OkResult( new
            {
                sents = sent,
                Message = error.ToString()
            });
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> UpdateLicence(int userId)
        {

            try
            {


                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.UserId == userId && !c.IsDeleted);

                if (user == null)
                {
                    return Bad("invalid request");
                }

                var status = await _unitOfWork.UserStatusRepository.Get(user.UserStatusId);
                if (status != null && status.Code.ToUpper() == "ACT")
                { }
                else
                {
                    return Bad("Account not active");
                }


                user.ActiveTill = DateTime.Now.AddDays(1);
               int userIdUpdated = await _unitOfWork.UserRepository.Update(user);

                List<string> emails = new List<string>();
                string[] emailKey = ConfigurationManager.AppSettings["EmailAddress"].Split(',');
                bool sent = false;
                if (emailKey != null)
                {
                    foreach (var item in emailKey)
                    {
                        emails.Add(item);
                    }
                }

                StringBuilder error = new StringBuilder("");
                StringBuilder message = new StringBuilder("");
                message.Append("Dear sir/madam");
                message.AppendLine();
                message.AppendLine();
                message.Append(user.FirstName + " " + user.LastName + " user number " + user.UserId + " has successfully paid for his account, please check account");
                message.AppendLine();
                message.AppendLine();

               // if (emails.Count > 0)
                   // sent = Utilities.SendEmail(emails, "Payment", message.ToString(), "", ref error);

                return OkResult(userIdUpdated > 0);

            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }

        }

        [HttpGet(), JwtAuthorize]
        public HttpResponseMessage DeviceValid(int userId, string serialNumber)
        {

            try
            {

                var devices =  _unitOfWork.DeviceRepository.GetList(c => c.UserId == userId && !c.IsDeleted);

                if (devices == null)
                {
                    return OkResult(new
                    {
                        Code = "APP",
                        Message = "Device is active"
                    });
                }

                var currentDevice = devices.FirstOrDefault(c => c.SerialNumber == serialNumber);

                if (currentDevice == null)
                {
                    return OkResult(new
                    {
                        Code = "APP",
                        Message = "Device is active"
                    });
                }

                 return OkResult(new
                {
                    Code = "APP",
                    Message = "Device is active"
                });

            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }

        }

        [HttpGet(), JwtAuthorize]
        public async Task<HttpResponseMessage> LicenceValid(int userId)
        {

            try
            {

                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.UserId == userId && !c.IsDeleted);

                if (user == null)
                {
                    return Bad("invalid request");
                }

                var status = await _unitOfWork.UserStatusRepository.Get(user.UserStatusId);
                if (status != null && status.Code.ToUpper() == "ACT")
                {
                    if(user?.ActiveTill.Value.Date >= DateTime.Now.Date)
                    {
                        return OkResult(new
                        {
                            Code = "APP",
                            Message = "Account active"
                        });
                    }
                    else
                    {
                        return Bad("Payment is required", "PAY");
                    }
                }
                else
                {
                    return Bad("Account not active", "DEC");
                }


            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }

        }

        [HttpGet(), JwtAuthorize]
        public async Task<HttpResponseMessage> GetMyDetails(int userId)
        {

            try
            {

               
                var user = await _unitOfWork.UserRepository.SingleOrDefault(c=>c.UserId == userId && !c.IsDeleted);

                if(user == null)
                {
                    return Bad("invalid request");
                }

                /*var createdBy = GetClaim("UserName");

                if(createdBy.ToLower() != user.EmailAddress.ToLower())
                    return Bad("invalid request");*/

                //Check user status
                var status = await _unitOfWork.UserStatusRepository.Get(user.UserStatusId);
                if (status != null && status.Code.ToUpper() == "ACT")
                { }
                else
                {
                    return Bad("Account not active");
                }

                var userDTO = Mapper.Map<UserDto>(user);


                return OkResult(userDTO);

            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }

        }

        [HttpGet(), JwtAuthorize]
        public  HttpResponseMessage GetTime(DateTime time)
        {
            if ((DateTime.Now - time).TotalDays > 2 || (DateTime.Now - time).TotalDays < -2)
            {
                return Bad("Please change your time");
            }

            return OkResult(true);
        }
        [HttpPost(), JwtAuthorize]
        public async Task<HttpResponseMessage> UpdateProfile([System.Web.Http.FromBody]UserDto userDto)
        {
            try
            {
                if (userDto == null || userDto.UserId == 0)
                    return Bad("All fields are required");

                var user = await _unitOfWork.UserRepository.Get(userDto.UserId);
                if (user == null || user.UserId == 0)
                    return Bad("We cant find your profile, please login");


                user.EmailAddress = userDto.EmailAddress;
                user.FirstName = userDto.FirstName;
                user.LastName = userDto.LastName;
                user.Gender = userDto.Gender;
                user.Mobile = userDto.Mobile;
                user.ModifiedDate = DateTime.Now;
                
                var changed = await _unitOfWork.UserRepository.Update(user);
                if (changed > 0)
                {
                    var results = new
                    {
                        code = "APP",
                        message = "Profile updated successfull"
                    };
                    return OkResult(results);
                }
                   

                return Bad("Profile could not be updated, please try again");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }


        [HttpPost(), JwtAuthorize]
        public async Task<HttpResponseMessage> AdminUpdateProfile([System.Web.Http.FromBody] UserDto userDto)
        {
            try
            {
                if (userDto == null || userDto.UserId == 0)
                    return Bad("All fields are required");

                var user = await _unitOfWork.UserRepository.Get(userDto.UserId);
                if (user == null || user.UserId == 0)
                    return Bad("We cant find your profile, please login");


                user.EmailAddress = userDto.EmailAddress;
                user.FirstName = userDto.FirstName;
                user.LastName = userDto.LastName;
                user.Gender = userDto.Gender;
                user.Mobile = userDto.Mobile;
                user.UserStatusId = userDto.UserStatusId;
                user.UserTypeId = userDto.UserTypeId;
                user.ActiveTill = userDto.ActiveTill;
                user.ModifiedDate = DateTime.Now;

                var changed = await _unitOfWork.UserRepository.Update(user);
                if (changed > 0)
                {
                    var results = new
                    {
                        code = "APP",
                        message = "Profile updated successfull"
                    };
                    return OkResult(results);
                }


                return Bad("Profile could not be updated, please try again");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> ResetPassword(string email, string code)
        {
            try
            {
                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => !c.IsDeleted && c.EmailAddress.ToLower() == email.ToLower());
                if (user != null && user.UserId > 0)
                {
                    if (code == user.VerificationCode)
                    {
                        string password = new Random().Next(111111111, 999999999).ToString();
                        byte[] passwordHash, passwordSalt;
                        Utilities.CreatePasswordHash(password, out passwordHash, out passwordSalt);

                        user.PasswordHash = passwordHash;
                        user.PasswordSalt = passwordSalt;
                        int userId = await _unitOfWork.Complete();
                        if (userId > 0)
                        {
                            List<string> emails = new List<string>();
                            emails.Add(user.EmailAddress);

                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.Append("<center><br/>");
                            stringBuilder.Append("Hi " + user.FirstName + "<br/><br/>");
                            stringBuilder.Append("Your new password is " + password + "<br/><br/>");
                            stringBuilder.Append("Please consider changing it under your profile.<br/><br/>");
                            stringBuilder.Append("If you have any queries, please contact us on the email below.<br/><br/>");
                            stringBuilder.Append("</center><br/><br/>");
                            StringBuilder error = new StringBuilder("");
                            bool sent = Utilities.SendEmail(emails, "Password", stringBuilder.ToString(), "", ref error);

                            if (sent)
                            {
                                var result = new
                                {
                                    code = "APP",
                                    message = "Password sent to your email"
                                };
                                return OkResult(result);
                            }
                            else
                            {
                                return Bad("Email was not sent, please contact us");
                            }
                        }
                    }
                    else
                    {
                        return Bad("Verification is not valid");
                    }
                }

                return Bad("No user with the details sent");
            }
            catch (Exception)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> SendEmailVerificationCode(string email)
        {
            try
            {
                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => !c.IsDeleted && c.EmailAddress.ToLower() == email.ToLower());
                if(user != null && user.UserId >0)
                {
                    user.VerificationCode = new Random().Next(1111, 9999).ToString();
                    int userId = await _unitOfWork.Complete();
                    if(userId > 0)
                    {
                        List<string> emails = new List<string>();
                        emails.Add(user.EmailAddress);
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append("<center><br/>");
                        stringBuilder.Append("Hi " + user.FirstName + "<br/><br/>");
                        stringBuilder.Append("Please use the code below to verify your change of password. <br/><br/>");
                        stringBuilder.Append(user.VerificationCode + "<br/><br/>");
                        stringBuilder.Append("If you have any queries, please contact us on the email below.<br/><br/>");
                        stringBuilder.Append("</center><br/><br/>");
                      
                      
                        StringBuilder error = new StringBuilder("");
                        bool sent = Utilities.SendEmail(emails, "Verification Code " + user.VerificationCode, stringBuilder.ToString(), "", ref error);

                        if (sent)
                        {
                            var result = new
                            {
                                code = "APP",
                                message = "Verification code sent your email."
                            };
                            return OkResult(result);
                        }
                        else
                        {
                            return Bad("Email was not sent, please contact us " + error.ToString());
                        }
                    }
                }

                return Bad("No user with the details sent");
            }
            catch (Exception)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpPost(), JwtAuthorize]
       
        public async Task<HttpResponseMessage> ChangePassword(ChangePassDto changePassDto)
        {
            try
            {
                if (string.IsNullOrEmpty(changePassDto.EmailAddress) || string.IsNullOrEmpty(changePassDto.CurrentPassword) || string.IsNullOrEmpty(changePassDto.NewPassword))
                    return Bad("All fields are required");


                //Check whether the user exists
                var changed = await _unitOfWork.UserRepository.ChangePassword(changePassDto.EmailAddress, changePassDto.CurrentPassword, changePassDto.NewPassword);
                if (!changed)
                    return Bad("Password could not be changed, make sure you use current correct password, please try again");

                var result = new
                {
                    code = "APP",
                    message = "Password changed successfull"
                };
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }


        }


        [HttpPost()]
    
        public  HttpResponseMessage Contact([System.Web.Http.FromBody]ContactDto contact)
        {
            try
            {

                StringBuilder error = new StringBuilder();
                StringBuilder message = new StringBuilder();
              
                message.Append(contact.Message);
                message.AppendLine();
                message.AppendLine();

                List<string> emails = new List<string>();
                string[] emailKey = ConfigurationManager.AppSettings["EmailAddress"].Split(',');
                bool sent = false;
                if (emailKey != null)
                {
                    foreach (var item in emailKey)
                    {
                        emails.Add(item);
                    }
                }

                /* 
                 emails.Add("claire@theoryofalignment.com");
                 emails.Add("daleen@theoryofalignment.com");*/
                 if(emails.Count > 0)
                    sent =  Utilities.SendEmail(emails, "Internet Email", message.ToString(), contact.EmailAddress, ref error);

                if (sent)
                {
                    var results = new
                    {
                        code = "APP",
                        message = "Profile updated successfull"
                    };
                    return OkResult(results);
                }
                else
                {
                    return Bad("Error occured, please try again");
                }

            }
            catch (Exception ex)
            {
                return Bad("Error occured, please try again");
            }

        }

        [HttpPost(), JwtAuthorize]
   
        public async Task<HttpResponseMessage> Save([System.Web.Http.FromBody]UserDto userDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Bad("All fields are required");
                

                var user = await _unitOfWork.UserRepository.Saver(userDto);
                if (user != null)
                {
                    var results = new
                    {
                        code = "APP",
                        userId = user.UserId,
                        message = "Saved successfull"
                    };

                    return OkResult(results);

                }
                return Bad("An error occured, please try again");

            }
            catch (Exception ex)
            {
                return Bad("Error occured, please try again");
            }

        }
        [HttpPost()]
        public async Task<HttpResponseMessage> Register([System.Web.Http.FromBody]RegisterDto userDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Bad("All fields are required");

                var userName = userDto.EmailAddress.ToLower();

                //Check whether the user exists
                var exists = await _unitOfWork.UserRepository.UserExist(userName, true);
                if (exists)
                {
                   
                    return Bad("User already registered");
                }

                ///Register the user

                var user = await _unitOfWork.UserRepository.Register(userName, userDto.Password, userDto.FirstName, userDto.LastName, userDto.MobileNumber);
                if (user != null)
                {
                    var results = new
                    {
                        code = "APP",
                        userId = user.UserId,
                        message = "Registered successfull"
                    };
                    StringBuilder error = new StringBuilder();
                    StringBuilder stringBuilder = new StringBuilder();
                    
                    stringBuilder.Append("<center><br/>");
                    stringBuilder.Append("Welcome " + user.FirstName + "<br/><br/>");
                    stringBuilder.Append("Please use the verification code below to start your reALIGNA Journey. <br/><br/>");
                    stringBuilder.Append(user.VerificationCode + "<br/><br/>");
                    stringBuilder.Append("If you have any queries, please contact us on the email below.<br/><br/>");
                    stringBuilder.Append("</center><br/><br/>");

                    List<string> emails = new List<string>();
                    emails.Add(userDto.EmailAddress);

                    Utilities.SendEmail(emails, "Verification", stringBuilder.ToString(),"", ref error);
                    return OkResult(results);

                }
                return Bad("Registration failed, please try again");

            }
            catch (Exception ex)
            {
                return Bad("Error occured, please try again" + ex.Message);
            }

        }

        [HttpPost()]
        public async Task<HttpResponseMessage> Verify([System.Web.Http.FromBody]UserVerifyDto userVerifyDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Bad("All fields are required");

                var userName = userVerifyDto.EmailAddress.ToLower();

                //Check whether the user exists
                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.EmailAddress == userName && !c.IsDeleted);
                if (user == null || user.UserId == 0)
                    return Bad("User doesnt exist");

                if (userVerifyDto.VerificationCode != user.VerificationCode)
                    return Bad("Verification code is not correct");

                var status = await _unitOfWork.UserStatusRepository.SingleOrDefault(c => !c.IsDeleted && c.Code.ToLower() == "act");

                user.UserStatusId = status.UserStatusId; // chnage status to active

                int id = await _unitOfWork.UserRepository.Update(user); // save changes


                if (id <= 0)
                    return Bad("Verification failed, please try again");

                return OkResult("APP");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }

        }

        [HttpGet(), JwtAuthorize]
        public async Task<HttpResponseMessage> GetUserDetails(int userId)
        {

            try
            {
                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.UserId == userId);
                if (user != null)
                {
                    var userDTO = Mapper.Map<UserDto>(user);
                    return OkResult(userDTO);
                }
                else
                {
                    return Bad("No user found");
                }

            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }

        }

        [HttpGet(), JwtAuthorize]
        public HttpResponseMessage GetUsers(int pageNumber, string search)
        {
            try
            {
                UserParam userParam = GetUserParam(pageNumber, search);
                var result = new List<Business.DB.User>();
                if (!string.IsNullOrEmpty(userParam.Search))
                    result = _unitOfWork.UserRepository.Find(c => c.FirstName.ToLower().Contains(userParam.Search.ToLower()) || c.LastName.ToLower().Contains(userParam.Search.ToLower()), userParam);
                else
                    result = _unitOfWork.UserRepository.Find(userParam);

                if (result != null)
                {
                    var items = Mapper.Map<List<UserDto>>(result);
                    return OkResult(items);
                }

                return Bad("No data");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }
        [HttpPost()]
        public async Task<HttpResponseMessage> Login([System.Web.Http.FromBody]UserLoginDto userDto)
        {

            try
            {
                if (!ModelState.IsValid)
                    return Bad("All fields are required");

                var userName = userDto.EmailAddress.ToLower();
                var user = await _unitOfWork.UserRepository.Login(userName, userDto.Password);
                if (user == null)
                {
                    return Bad("Not authorised");
                }

                //Check user status
                var status = await _unitOfWork.UserStatusRepository.Get(user.UserStatusId);
                if (status != null && status.Code.ToUpper() == "ACT")
                { }
                else
                {
                    return Bad("Please verify your account first");
                }

                //device

                var devices = _unitOfWork.DeviceRepository.GetList(c => c.UserId == user.UserId && !c.IsDeleted);

                if(!string.IsNullOrEmpty(userDto.SerialNumber))
                {
                    bool save = false;
                    if (devices == null || devices.Count() == 0)
                        save = true;
                    else 
                    {
                        var existing = devices.FirstOrDefault(c => c.SerialNumber == userDto.SerialNumber);

                        if (devices.Count() < 2)
                        {
                            if(existing == null)
                               save = true;
                        }
                        else
                        {
                           /// return Bad("You have reached maximum devices you can use to login.");
                        }
                    }
                    if(save)
                    {
                        Device device = new Device();
                        device.SerialNumber = userDto.SerialNumber;
                        device.CreatedBy = user.UserId;
                        device.ModifiedBy = user.UserId;
                        device.CreatedDate = DateTime.Now;
                        device.ModifiedDate = DateTime.Now;
                        device.UserId = user.UserId;
                        device.DeviceOS = userDto.DeviceOs;
                        device.Description = userDto.Description;
                        _unitOfWork.DeviceRepository.Add(device);
                       await _unitOfWork.Complete();

                    }
                }


                string Secret = ConfigurationManager.AppSettings["AuthenticationSecretKey"];

                // var key = new SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(Secret));
                // var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);
                var symmetricKey = Convert.FromBase64String(Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new[]
                       {
                        new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                         new Claim(ClaimTypes.Name, user.EmailAddress)
                    }),

                    Expires = DateTime.Now.AddYears(1),

                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
                };
             

                var tokenHandler = new JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);

               var userDTO = Mapper.Map<UserDto>(user);
                userDTO.Token = tokenHandler.WriteToken(token);


                return OkResult(userDTO);

            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again " + ex.Message);
            }

        }

        [HttpPost]
        async public Task<HttpResponseMessage> SetCustomerProfile(User userDto)
        {
            try
            {
                var createdBy = GetClaim("UserName");
                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.EmailAddress == userDto.EmailAddress);

                if (user == null)
                {
                    return Bad("User  does not exist");
                }

                user.FirstName = userDto.FirstName;
                user.LastName = userDto.LastName;
                user.Mobile = userDto.Mobile;
                int update = await _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.Complete();
                return OkResult(user);


            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }


        [HttpGet, HttpPost]
        public HttpResponseMessage Search(int pageNumber, int pageSize, string q)
        {
            try
            {
                var result = new PageList<User>(new List<User>(), 1, 1, 10);

                if (!string.IsNullOrEmpty(q))
                {
                    result = _unitOfWork.UserRepository.FindWithInclude(c => c.FirstName.ToLower().Contains(string.IsNullOrEmpty(q) ? "" : q.ToLower()), new UserParam() { PageNumber = pageNumber, PageSize = pageSize, Search = string.IsNullOrEmpty(q) ? "" : q.ToLower() }, p=>p.UserStatus);

                }
                else
                {
                    result = _unitOfWork.UserRepository.FindWithInclude(c => c.IsDeleted == false, new UserParam() { PageNumber = pageNumber, PageSize = pageSize }, p => p.UserStatus);
                }

                if (result != null)
                {
                    return OkResult(result.ToPagedMapper<UserDto, User>());
                }

                return Bad("No data");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpPost]
        async public Task<HttpResponseMessage> SetUserStatus(UserStatusRequestDto userDto)
        {
            try
            {
                var createdBy = GetClaim("UserName");
                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.UserId == userDto.UserId);

                if (user == null)
                {
                    return Bad("User  does not exist");
                }

                var userStatus = _unitOfWork.UserStatusRepository.SingleOrDefault(c => c.UserStatusId == userDto.UserStatusId);

                if (userStatus == null)
                {
                    return Bad("User status  does not exist");
                }
                user.UserStatusId = userDto.UserStatusId;
                int update = await _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.Complete();
                return OkResult(true);


            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }
    }
}