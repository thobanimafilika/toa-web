﻿
using AutoMapper;
using Musiq.Business.Core;
using Musiq.Common;
using Musiq.Common.Dto;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.IdentityModel.Tokens;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Musiq.Controllers
{
    public class AuthController : BaseController
    {
        IUnitOfWork _unitOfWork;
        public AuthController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpPost()]
        public async Task<HttpResponseMessage> Login([System.Web.Http.FromBody]UserLoginDto userDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Bad("All fields are required");

                var userName = userDto.EmailAddress.ToLower();
                var user = await _unitOfWork.UserRepository.Login(userName, userDto.Password);
                if (user == null)
                {
                    return Bad("Not authorised");
                }

                //Check user status
                var status = await _unitOfWork.UserStatusRepository.Get(user.UserStatusId);
                if (status == null)
                {

                }
                else if (status.Code.ToUpper() == "ACT")
                {

                }
                else
                {
                    return Bad("Please verify your account first");
                }

                //Check user status
                var userType = await _unitOfWork.UserTypeRepository.Get(user.UserTypeId);
                if (userType == null)
                {
                    return Bad("failed getting user type");
                }


                var claims = new[]
                {
                new Claim(ClaimTypes.NameIdentifier, user.UserId.ToString()),
                new Claim(ClaimTypes.Name, user.EmailAddress)

                };

                string Secret = ConfigurationManager.AppSettings["AuthenticationSecretKey"];

                var symmetricKey = Convert.FromBase64String(Secret);
                var key = new SymmetricSecurityKey(symmetricKey);
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.Now.AddDays(2),
                    SigningCredentials = creds

                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);

                var userDTO = Mapper.Map<UserDto>(user);
                userDTO.Token = tokenHandler.WriteToken(token);
                userDTO.UserTypeCode = userType.Code;
                return OkResult(userDTO);

            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }

        }


        [HttpPost()]
        public async Task<HttpResponseMessage> Register([System.Web.Http.FromBody]RegisterDto userDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Bad("All fields are required");

                var userName = userDto.EmailAddress.ToLower();

                //Check whether the user exists
                var exists = await _unitOfWork.UserRepository.UserExist(userName, true);
                if (exists)
                    return Bad("User already registered");

                ///Register the user

                var user = await _unitOfWork.UserRepository.Register(userName, userDto.Password, userDto.FirstName, userDto.LastName, "");
                if (user != null)
                {

                    string myLink = System.Configuration.ConfigurationManager.AppSettings["PortalBaseUrl"].ToString() + $"/Auth/RegisterVerifyCode/?email={user.EmailAddress}&code=" + user.VerificationCode;

                    var message = $"Hi {user.FirstName} { user.LastName }<br/> Click <a href='{myLink}'> here </a> or below to verify your account or paste link to browser {myLink} ";

                    var emailFaileSentError = "";
                    var sendEmail = Email.SendEmail(user.EmailAddress, "New Account .Theory Of Alignement", message, "New Account ", ref emailFaileSentError);

                    var results = new
                    {
                        code = "APP",
                        userId = user.UserId,
                        message = "Registered successfull"
                    };

                    return OkResult(results);

                }
                return Bad("Registration failed, please try again");

            }
            catch (Exception ex)
            {
                return Bad("Error occured, please try again" + ex.Message);
            }
        }

        [HttpPost()]
        public async Task<HttpResponseMessage> ContactUs([System.Web.Http.FromBody]ContactUsDto userDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return Bad("All fields are required");


                if (userDto != null)
                {
                    char[] spearator = { ',', ' ' };
                    string[] myLink = System.Configuration.ConfigurationManager.AppSettings["EmailForwardTo"].ToString().Split(spearator);

                    if (myLink != null)
                    {

                        foreach (String email in myLink)
                        {
                            var message = $"Hi <br/> Good day attached below is the email from a web visitor <br/> Name :{ userDto.Name } <br/> Phone : {userDto.Phone}<br/> Email : {userDto.Email} <br> Title :{ userDto.Title } <br> Body : {userDto.HowDidYouHearAboutUs}";

                            var emailFaileSentError = "";
                            var sendEmail = Email.SendEmail(email, "Web visitor .Theory Of Alignement", message, "Web visitor inquiry ", ref emailFaileSentError);

                        }
                    }

                    return OkResult(new { Message = "Email has been sent" });

                }
                return Bad("Registration failed, please try again");

            }
            catch (Exception ex)
            {
                return Bad("Error occured, please try again" + ex.Message);
            }
        }



        [HttpGet()]
        public async Task<HttpResponseMessage> RegisterVerifyCode(string code, string email)
        {
            try
            {
                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.VerificationCode == code && c.EmailAddress == email, p => p.UserStatus);
                if (user == null)
                    return BadRequest("Verification code doesnt exists ,  try again");

                if (user.UserStatus.Code.ToLower() == "act")
                    return BadRequest("User already activated");

                var activeCustomerStatus = await _unitOfWork.UserStatusRepository.SingleOrDefault(c => c.Code == "act");

                if (activeCustomerStatus == null)
                {
                    return BadRequest("User status not found contact service provider ");
                }
                user.UserStatusId = activeCustomerStatus.UserStatusId;
                var setToActive = await _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.Complete();
                return OkResult("User Account verified ");
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> ForgotPasswordSendEmailToken(string email)
        {
            try
            {
                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.EmailAddress == email, p => p.UserStatus);
                if (user == null)
                    return BadRequest("user doesnt exists ,  try again");

                if (user.UserStatus.Code.ToLower() == "act" || user.UserStatus.Code.ToLower() == "pen")
                {

                }
                else
                {
                    return BadRequest("please contact site admin to get your activate");
                }

                var activeCustomerStatus = await _unitOfWork.UserStatusRepository.SingleOrDefault(c => c.Code == "act");

                if (activeCustomerStatus == null)
                {
                    return BadRequest("User status not found contact service provider ");
                }
                user.UserStatusId = activeCustomerStatus.UserStatusId;
                var setToActive = _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.Complete();
                return OkResult("User Account verified ");
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }



        [HttpGet()]
        public async Task<HttpResponseMessage> IsForgotPasswordExist(string code)
        {
            try
            {
                var token = await _unitOfWork.ForgotPasswordRepository.SingleOrDefault(c => c.ForgotToken == code);
                if (token == null)
                    return BadRequest("token doesnt exists ,  try again");
                else
                    return OkResult("token doesnt exists ,  try again");

            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }

    }
}
