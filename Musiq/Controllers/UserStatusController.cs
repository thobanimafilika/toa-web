﻿using AutoMapper;
using Musiq.App_Start;
using Musiq.Business.Core;
using Musiq.Common;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Musiq.Controllers
{
    [RoutePrefix("api/[controller]"), JwtAuthorize]
    public class UserStatusController : BaseController
    {
        IUnitOfWork _unitOfWork;
        public UserStatusController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> Get()
        {
            try
            {
                var result = await _unitOfWork.UserStatusRepository.GetAll();
                if (result != null)
                {
                    var items = Mapper.Map<List<UserStatusDto>>(result);
                    return OkResult(items);
                }

                return Bad("no data");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }
    }
}
