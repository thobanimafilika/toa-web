﻿using AutoMapper;
using Musiq.App_Start;
using Musiq.Business.Core;
using Musiq.Business.DB;
using Musiq.Common;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Musiq.Controllers
{
    [RoutePrefix("api/[controller]"), JwtAuthorize]
    public class CommentController : BaseController
    {
        IUnitOfWork _unitOfWork;
        public CommentController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet()]
        public HttpResponseMessage GetComments(int postId, int pageNumber)
        {
            try
            {
                var result =  _unitOfWork.CommentRepository.Find(c => c.PostId == postId, GetUserParam(pageNumber));
                if (result != null)
                {
                    var comments = Mapper.Map<List<CommentDto>>(result);
                    return OkResult(comments);
                }
                return BadRequest("No comments");
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }


        }

        [HttpDelete()]
        public async Task<HttpResponseMessage> Delete(int commentId)
        {
            try
            {

                var result = await _unitOfWork.CommentRepository.SingleOrDefault(c => c.CommentId == commentId);
                if (result != null)
                {
                    result.IsDeleted = true;
                    int id = await _unitOfWork.CommentRepository.Update(result);
                    if (id > 0)
                        return OkResult("Deleted");
                }

                return BadRequest("Comment could not be deleted");
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }

        [HttpPost()]
        public async Task<HttpResponseMessage> Save([System.Web.Http.FromBody]CommentDto commentDto)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest("Some fields are required");

                Comment comment = new Comment();
                
                comment.CreatedDate = DateTime.Now;
                comment.CreatedBy = commentDto.CreatedBy;
                comment.Email = commentDto.Email;
                comment.Description = commentDto.Description;
                comment.LastName = commentDto.LastName;
                comment.Name = commentDto.Name;
                comment.PostId = commentDto.PostId;
                
                _unitOfWork.CommentRepository.Add(comment);
                int Id = await _unitOfWork.Complete();
                if (Id > 0)
                    return OkResult("Saved");
                else
                    return BadRequest("An error occured, please try again");
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }




    }
}