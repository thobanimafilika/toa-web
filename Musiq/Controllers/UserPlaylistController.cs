﻿using AutoMapper;
using Musiq.App_Start;
using Musiq.Business.Core;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Musiq.Controllers
{
    //[RoutePrefix("api/[controller]"), JwtAuthorize]
    [RoutePrefix("api/[controller]")]
    public class UserPlaylistController : BaseController
    {
        IUnitOfWork _unitOfWork;
        public UserPlaylistController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// This only works with user ending session
        /// </summary>
        /// <param name="categoryCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet()]
        public async Task<HttpResponseMessage> GetNext(string categoryCode, int userId)
        {
            try
            {
                if(string.IsNullOrEmpty(categoryCode) || userId == 0) 
                {
                    return BadRequest("all fiels are required");
                }
                var category = await _unitOfWork.CategoryRepository.SingleOrDefault(c => c.Code.ToLower() == categoryCode.ToLower());

                if (category == null)
                {
                    return BadRequest("no categories");
                }

                var playlistId = await _unitOfWork.UserPlaylistRepository.NextPlayList(category.CategoryId, userId);
               return OkResult(new { PlaylistId = playlistId });


            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }

        /// <summary>
        /// This works with users ending session and also 
        /// session end every 8 hours
        /// </summary>
        /// <param name="categoryCode"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet()]
        public async Task<HttpResponseMessage> GetUserNextPlayList(string categoryCode, int userId)
        {
            try
            {
                if (string.IsNullOrEmpty(categoryCode) || userId == 0)
                {
                    return BadRequest("all fiels are required");
                }
                var category = await _unitOfWork.CategoryRepository.SingleOrDefault(c => c.Code.ToLower() == categoryCode.ToLower());

                if (category == null)
                {
                    return BadRequest("no categories");
                }

                var playlistId = await _unitOfWork.UserPlaylistRepository.NextUserPlayList(category.CategoryId, userId);
                return OkResult(new { PlaylistId = playlistId });


            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }


        [HttpGet()]
        public async Task<HttpResponseMessage> PlayingCurrentPlaylist(int playListId, int userId)
        {
            try
            {
                var currentUserPlaylist =  _unitOfWork.UserPlaylistRepository.LastUserPlayList(playListId, userId);
                if(currentUserPlaylist != null)
                {
                    currentUserPlaylist.PlayedDate = DateTime.Now;
                    currentUserPlaylist.Playing = true;
                    int Id = await _unitOfWork.UserPlaylistRepository.Update(currentUserPlaylist);
                    if(Id > 0)
                    {
                        return OkResult(new
                        {
                            Code = "APP",
                            Message = "Playlist update to playing"
                        });
                    }
                }


                return BadRequest("Current playlist not updated");
                //var playlistId = await _unitOfWork.UserPlaylistRepository.NextUserPlayList(category.CategoryId, userId);
                //return OkResult(new { PlaylistId = playlistId });


            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }


        [HttpPost()]
        public async Task<HttpResponseMessage> Update([System.Web.Http.FromBody] UserPlaylistUpdateDto  userPlaylistUpdateDto)
        {
            try
            {
                if(userPlaylistUpdateDto == null || userPlaylistUpdateDto.PlaylistId == 0 || userPlaylistUpdateDto.UserId == 0)
                {
                    return BadRequest("all fiels are required");
                }

                var userPlaylist = await _unitOfWork.UserPlaylistRepository.SingleOrDefault(c => c.PlaylistId == userPlaylistUpdateDto.PlaylistId && !c.Finished && c.UserId == userPlaylistUpdateDto.UserId);
                if (userPlaylist != null)
                {
                    userPlaylist.Finished = true;
                    var id = await _unitOfWork.Complete();

                    return OkResult(new
                    {
                        Code = "APP",
                        Message = "Playlist update"
                    });

                }

                return Bad("Playlist not updated");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpPost()]
        public async Task<HttpResponseMessage> TurnONorOff([System.Web.Http.FromBody] UserPlaylistItemDto userPlaylistItemDto)
        {
            try
            {
                if(userPlaylistItemDto != null && userPlaylistItemDto.PlaylistItemId > 0 && userPlaylistItemDto.UserId > 0)
                {
                    bool update = false;
                    var userPlaylistItem = await _unitOfWork.UserPlaylistItemRepository.SingleOrDefault(c => c.UserId == userPlaylistItemDto.UserId && c.PlaylistItemId == userPlaylistItemDto.PlaylistItemId);
                    if(userPlaylistItem == null)
                    {
                        userPlaylistItem = new Business.DB.UserPlaylistItem();
                        userPlaylistItem.EnabledDate = userPlaylistItemDto.EnabledDate;
                        userPlaylistItem.CreatedBy = userPlaylistItemDto.UserId;
                        userPlaylistItem.CreatedDate = DateTime.Now;
                    }
                    else
                    {
                        update = true;
                        userPlaylistItem.ModifiedBy = userPlaylistItemDto.UserId;
                        userPlaylistItem.CreatedDate = DateTime.Now;
                    }
                    userPlaylistItem.PlaylistItemId = userPlaylistItemDto.PlaylistItemId;
                    userPlaylistItem.Enabled = userPlaylistItemDto.Enabled;
                    userPlaylistItem.UserId = userPlaylistItemDto.UserId;
                    int id = 0;
                    if (update)
                        id = await _unitOfWork.UserPlaylistItemRepository.Update(userPlaylistItem);
                    else
                    {
                       _unitOfWork.UserPlaylistItemRepository.Add(userPlaylistItem);
                        id = await _unitOfWork.Complete();
                    }

                    if(id > 0)
                    {
                        return OkResult(new
                        {
                            Code = "APP",
                            Message = "Playlist update"
                        });

                    }
                    else
                    {
                        return Bad("Playlist could not be updated");
                    }

                }
                return Bad("Playlist could not be updated");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again ");
            }
        }


        [HttpGet()]
        public async Task<HttpResponseMessage> GetEnabledPlaylist(int userId, int playlistId)
        {
            try
            {

                var playlist = await _unitOfWork.UserPlaylistItemRepository.GetEnabledPlaylist(userId, playlistId);

                return OkResult(playlist);

            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }

    }
}