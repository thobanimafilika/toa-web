﻿using AutoMapper;
using Musiq.App_Start;
using Musiq.Business.Core;
using Musiq.Business.DB;
using Musiq.Common;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Musiq.Controllers
{
   // [RoutePrefix("api/[controller]"), JwtAuthorize]
    [RoutePrefix("api/[controller]")]
    public class DashboardController : BaseController
    {
        IUnitOfWork _unitOfWork;
        public DashboardController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> Admin()
        {
            try
            {
                var result = await _unitOfWork.DashboardRepository.GetAdminDashboard() ?? new { };
              
                 return OkResult(result); 
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }
    
    }
}