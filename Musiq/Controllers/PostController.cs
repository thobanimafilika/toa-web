﻿using AutoMapper;
using Musiq.App_Start;
using Musiq.Business.Core;
using Musiq.Business.DB;
using Musiq.Common;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Musiq.Controllers
{
    [RoutePrefix("api/[controller]"), JwtAuthorize]
    public class PostController : BaseController
    {

        IUnitOfWork _unitOfWork;
        public PostController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet()]
        public  HttpResponseMessage GetPosts(int pageNumber, string search)
        {
            try
            {
                UserParam userParam = GetUserParam(pageNumber, search);
                var result = _unitOfWork.PostRepository.Find(c => c.Title.ToLower().Contains(userParam.Search.ToLower()) || c.Description.ToLower().Contains(userParam.Search.ToLower()), userParam);

                if (result != null)
                {
                    var items = Mapper.Map<List<PostDto>>(result);
                    return OkResult(items);
                }

                return BadRequest("No data");
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> GetPost(int postId)
        {
            try
            {
               
                var result = await _unitOfWork.PostRepository.SingleOrDefault(c => c.PostId == postId, b=>b.Comments);
                return OkResult(result);
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }

        [HttpPost()]
        public async Task<HttpResponseMessage> Save([System.Web.Http.FromBody]PostDto postDto )
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest("All fieds are required");

                Post post = new Post();
                if(postDto.PostId > 0)
                {
                    post = await _unitOfWork.PostRepository.SingleOrDefault(c => c.PostId == postDto.PostId);
                }
                
                if(post == null)
                {
                    post = new Post();
                    post.CreatedDate = DateTime.Now;
                    post.CreatedBy = postDto.CreatedBy;
                }
                else
                {
                    post.ModifiedDate = DateTime.Now;
                    post.ModifiedBy = postDto.CreatedBy;
                }
                
                post.Title = postDto.Title;
                post.Description = postDto.Description;

                int id = 0;
                if(post.PostId > 0)
                {
                   _unitOfWork.PostRepository.Add(post);
                  id=  await _unitOfWork.Complete();
                }else
                {
                  id= await _unitOfWork.PostRepository.Update(post);
                }
                if (id > 0)
                    return OkResult("Saved");
                else
                    return BadRequest("An error occured, please try again");
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }

        [HttpDelete()]
        public async Task<HttpResponseMessage> Delete(int postId)
        {
            try
            {

                var result = await _unitOfWork.PostRepository.SingleOrDefault(c => c.PostId == postId);
                if(result != null)
                {
                    result.IsDeleted = true;
                   int id = await _unitOfWork.PostRepository.Update(result);
                    if (id > 0)
                        return OkResult("Deleted");
                }

                return BadRequest("Post could not be deleted");
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }
    }
}
