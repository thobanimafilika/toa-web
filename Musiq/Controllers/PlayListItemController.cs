﻿using AutoMapper;
using Musiq.App_Start;
using Musiq.Business.Core;
using Musiq.Business.DB;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Musiq.Controllers
{
    [RoutePrefix("api/[controller]"), JwtAuthorize]
    public class PlayListItemController : BaseController
    {
        IUnitOfWork _unitOfWork;
        public PlayListItemController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> SyncPlaylistItems()
        {
            try
            {
                
                var result =  await _unitOfWork.PlayListItemRepository.GetList();
                if (result != null)
                {
                    return OkResult(result);
                   
                }
                return Bad("no results");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }


        [HttpGet()]
        public async Task<HttpResponseMessage> GetPlayList(int playlistid)
        {
            try
            {
                var result = await _unitOfWork.PlayListItemRepository.GetListByPlayListId(playlistid) ?? new List<Common.Dto.PlayListItemProductDto>();

                return OkResult(result);
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }


        [HttpPost()]
        public async Task<HttpResponseMessage> Save([System.Web.Http.FromBody]PlayListItemDto playListDto)
        {
            try
            {
                if (playListDto == null)
                    return Bad("All fieds are required");

                var playList = Mapper.Map<PlayListItem>(playListDto);

                if (playList.PlayListItemId == 0)
                {
                    playList.CreatedDate = DateTime.Now;
                    playList.CreatedBy = 4;
                }
                else
                {
                    playList.ModifiedDate = DateTime.Now;
                    playList.ModifiedBy = 4;
                }
                var product = await _unitOfWork.ProductRepository.Get(playList.ProductId);
                if (product != null)
                {
                    playList.Title = product.Title;
                    playList.Description = product.Description;
                }
                else
                {
                    throw new Exception("Product does not exist");
                }
                if (playList.PlayListItemId <= 0)
                {
                    _unitOfWork.PlayListItemRepository.Add(playList);
                }
                else
                {
                    await _unitOfWork.PlayListItemRepository.Update(playList);
                }
                await _unitOfWork.Complete();
                if (playList?.ProductId > 0)
                    return OkResult(playList?.PlayListItemId ?? -1);
                else
                    return Bad("An error occured, please try again");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }


        [HttpGet()]
        public async Task<HttpResponseMessage> Delete(int playListItemId = -1)
        {
            try
            {
                var result = await _unitOfWork.PlayListItemRepository.SingleOrDefault(c => c.PlayListItemId == playListItemId);
                if (result != null)
                {
                    result.IsDeleted = true;
                    var product = await _unitOfWork.PlayListItemRepository.Update(result);
                    await _unitOfWork.Complete();
                    return OkResult("Playlist item deleted successfully");
                }
                return Bad("playlist item doesn't exists");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again" + ex.Message);
            }
        }
    }
}