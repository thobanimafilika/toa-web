﻿using AutoMapper;
using Musiq.App_Start;
using Musiq.Business.Core;
using Musiq.Business.DB;
using Musiq.Common;
using Musiq.Common.Dto;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Musiq.Controllers
{
    [RoutePrefix("api/[controller]"), JwtAuthorize]
    public class TransactionController : BaseController
    {
        IUnitOfWork _unitOfWork;
        public TransactionController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet()]
        public HttpResponseMessage Get(int pageNumber)
        {
            try
            {
                var result = _unitOfWork.TransactionRepository.Find(GetUserParam(pageNumber), c=>c.TransactionStatus);
                if (result != null)
                {
                    var comments = Mapper.Map<List<TransactionDto>>(result);
                    return OkResult(comments);
                }
                return BadRequest("No comments");
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }


        }

        



       /* public static HttpClient client()
        {
            // Creating a sandbox environment
            PayPalEnvironment environment = new SandboxEnvironment(clientId, secret);

            // Creating a client for the environment
            PayPalHttpClient client = new PayPalHttpClient(environment);
            return client;
        }*/




    }
}