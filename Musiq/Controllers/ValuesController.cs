﻿using Musiq.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Musiq.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {

            List<string> em = new List<string>()
            {
                "billywatsy@gmail.com"
            };
            var bbb = new System.Text.StringBuilder();
            Utilities.SendEmail(em, "test", "we are testing", "", ref bbb);
            return new string[] { "value1", "value2", bbb.ToString() };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
