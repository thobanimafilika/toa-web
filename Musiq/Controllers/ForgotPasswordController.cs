﻿using AutoMapper;
using Musiq.App_Start;
using Musiq.Business.Core;
using Musiq.Common;
using Musiq.Common.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Musiq.Controllers
{
    public class ForgotPasswordController : BaseController
    {
        IUnitOfWork _unitOfWork;
        public ForgotPasswordController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet()]
        public async Task<HttpResponseMessage> RequestForgotPasswordToken(string email)
        {
            try
            {
                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.EmailAddress == email);
                if (user == null)
                    return BadRequest("Email does not exist,  try again");


                string token = StringHelper.RandomString(190, true);

                var isUserExist = await _unitOfWork.ForgotPasswordRepository.SingleOrDefault(c => c.UserId == user.UserId);
                if (isUserExist == null)
                {
                    var userToken = new Business.DB.ForgotPassword()
                    {
                        ForgotDate = DateTime.Now,
                        ForgotToken = token,
                        UserId = user.UserId
                    };
                    _unitOfWork.ForgotPasswordRepository.Add(userToken);

                    await _unitOfWork.Complete();
                    isUserExist = await _unitOfWork.ForgotPasswordRepository.SingleOrDefault(c => c.UserId == user.UserId);
                }
                isUserExist.ForgotToken = token;
                isUserExist.ForgotDate = DateTime.Now;

                string myLink = System.Configuration.ConfigurationManager.AppSettings["PortalBaseUrl"].ToString() + "/ForgotPassword/VerifyCode/?token=" + token;

                var message = $"Hi {user.FirstName} {user.LastName }<br/> Click <a href='{myLink}'> here </a> or below to change password paste list to browser {myLink}";

                var emailFaileSentError = "";
                var sendEmail = Email.SendEmail(user.EmailAddress, "Forgot Password", message, "Forgot Password" , ref emailFaileSentError);

                if (sendEmail)
                {
                    await _unitOfWork.ForgotPasswordRepository.Update(isUserExist);
                    await _unitOfWork.Complete();


                    return OkResult("Email has been sent ");
                }
                else
                {

                    return BadRequest($"email failed sending or {emailFaileSentError}");
                }
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }


        [HttpGet()]
        public async Task<HttpResponseMessage> IsForgotPassworTokenExist(string token)
        {
            try
            {
                var user = await _unitOfWork.ForgotPasswordRepository.SingleOrDefault(c => c.ForgotToken == token);
                if (user == null)
                    return BadRequest("token does not exist,  try again");
                else
                    return OkResult("Token Exist");

            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }

        [HttpPost()]
        public async Task<HttpResponseMessage> ChangePasswordByToken(string token , string password)
        {
            try
            {
                var userForgot = await _unitOfWork.ForgotPasswordRepository.SingleOrDefault(c => c.ForgotToken == token);
                if (userForgot == null)
                    return BadRequest("token does not exist,  try again");


                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.UserId == userForgot.UserId);
                if (user == null)
                    return BadRequest("failed getting user ");

                if(userForgot.ForgotDate <= DateTime.Now.AddDays(-2))
                {
                    return BadRequest("token has expired ");
                }

                byte[] passwordHash, passwordSalt;
                Utilities.CreatePasswordHash(password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;

                await _unitOfWork.UserRepository.Update(user);
                userForgot.ForgotToken = Guid.NewGuid().ToString();
                await _unitOfWork.ForgotPasswordRepository.Update(userForgot);
                await _unitOfWork.Complete();
                return OkResult("Password changed successfully");
            }
            catch (Exception ex)
            {
                return BadRequest("An error occured, please try again");
            }
        }
    }
}
