﻿using AutoMapper;
using Musiq.App_Start;
using Musiq.Business.Core;
using Musiq.Business.DB;
using Musiq.Common;
using Musiq.Common.Dto;
using Musiq.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Musiq.Controllers
{
    [RoutePrefix("api/[controller]"), JwtAuthorize]
    public class ProductController : BaseController
    {

        IUnitOfWork _unitOfWork;
        public ProductController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet()]
        public async Task<HttpResponseMessage> SyncProducts()
        {
            try
            {
                var status = await _unitOfWork.ProductStatusRepository.SingleOrDefault(c => c.Code.ToLower() == "act");
                var result = _unitOfWork.ProductRepository.GetList(c => !c.IsDeleted && status.ProductStatusId == status.ProductStatusId);
                if (result != null)
                {
                    var items = Mapper.Map<List<ProductDto>>(result);
                    return OkResult(items);
                }
                return Bad("no results");
                
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpGet()]
        public HttpResponseMessage Search(int pageNumber, int pageSize, string q)
        {
            try
            {
                var result = new PageList<Product>(new List<Product>(), 1, 1, 10);

                if (!string.IsNullOrEmpty(q))
                {
                    result = _unitOfWork.ProductRepository.Find(c => c.Title.ToLower().Contains(string.IsNullOrEmpty(q) ? "" : q.ToLower()), new UserParam() { PageNumber = pageNumber, PageSize = pageSize, Search = string.IsNullOrEmpty(q) ? "" : q.ToLower() });

                }
                else
                {
                    result = _unitOfWork.ProductRepository.Find(c => c.IsDeleted == false, new UserParam() { PageNumber = pageNumber, PageSize = pageSize });
                }

                if (result != null)
                {
                    return OkResult(result.ToPagedMapper<ProductDto, Product>());
                }

                return Bad("No data");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }



        [HttpGet()]
        public async Task<HttpResponseMessage> GetProducts(int pageNumber, string search)
        {
            try
            {
                var createdBy = GetClaim("UserName");
                var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.EmailAddress == createdBy, b => b.UserType, e => e.UserStatus);
                if (user == null)
                    return Bad("User doesnt exists");

                if (user.UserStatus.Code.ToLower() != "act")
                    return Bad("User doesnt deactivated");

                if (user.UserType.Code.ToLower() == "cus" && DateTime.Now > user.ActiveTill)
                    return Bad("Not authorised to view this audio");

                UserParam userParam = GetUserParam(pageNumber, search);
                var result = _unitOfWork.ProductRepository.Find(c => c.Title.ToLower().Contains(userParam.Search.ToLower()) || c.Description.ToLower().Contains(userParam.Search.ToLower()), userParam);
                if (result != null)
                {
                    var product = Mapper.Map<List<ProductDto>>(result);
                    return OkResult(product);
                }

                return Bad("No products found");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> GetProduct(int productId)
        {
            try
            {
                //var createdBy = GetClaim("UserName");
                //var user = await _unitOfWork.UserRepository.SingleOrDefault(c => c.EmailAddress == createdBy, b=>b.UserType, e=>e.UserStatus);
                //if (user == null)
                //    return Bad("User doesnt exists");

                //if(user.UserStatus.Code.ToLower() !="act")
                //    return Bad("User doesnt deactivated");

                //if(user.UserType.Code.ToLower() =="cus" && DateTime.Now > user.ActiveTill)
                //    return Bad("Not authorised to view this audio");

                var result = await _unitOfWork.ProductRepository.SingleOrDefault(c => c.ProductId == productId);
                if (result != null)
                {
                    //Mapper.Map<List<ProductStatusDto>>(allProduct);
                    var product = Mapper.Map<ProductDto>(result);
                    return OkResult(product);
                }

                return Bad("product doesn't exists");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpPost()]
        public async Task<HttpResponseMessage> Save([System.Web.Http.FromBody]ProductDto productDto)
        {
            try
            {
                if (productDto == null)
                    return Bad("All fieds are required");

                var product = Mapper.Map<Product>(productDto);

                if (product.ProductId == 0)
                {
                    product.CreatedDate = DateTime.Now;
                    product.CreatedBy = productDto.CreatedBy;
                }
                else
                {
                    product.ModifiedDate = DateTime.Now;
                    product.ModifiedBy = productDto.CreatedBy;
                }

                if (product.Hour > 24 || product.Hour < 0)
                {
                    return Bad("Hour should be between 0-24 hours");
                }

                if (product.Minute > 60 || product.Minute < 0)
                {
                    return Bad("Minutes should be between 0-60 minutes");
                }

                if (product.Second > 60 || product.Second < 0)
                {
                    return Bad("Second should be between 0-24 seconds");
                }
                
                if (product.Link == null)
                {
                    product.Link = "default.mp3";
                }

                product.CreatedBy = 4;

                int id = 0;
                if (product.ProductId <= 0)
                {
                    _unitOfWork.ProductRepository.Add(product);
                }
                else
                {
                    await _unitOfWork.ProductRepository.Update(product);
                }
                await _unitOfWork.Complete();
                if (product?.ProductId > 0)
                    return OkResult(product?.ProductId ?? -1);
                else
                    return Bad("An error occured, please try again");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> Delete(int productId = -1)
        {
            try
            {
                var result = await _unitOfWork.ProductRepository.SingleOrDefault(c => c.ProductId == productId);
                if (result != null)
                {
                    var isProductBelongToAPlayList = _unitOfWork.PlayListItemRepository.GetList(c => c.ProductId == productId);

                    if (isProductBelongToAPlayList == null || isProductBelongToAPlayList?.Count() <= 0)
                    {
                        result.IsDeleted = true;
                        int id = await _unitOfWork.ProductRepository.Update(result);
                        await _unitOfWork.Complete();
                        return OkResult("Deleted");
                    }
                    var list = string.Join(" , ", isProductBelongToAPlayList.Select(c => c.Title).ToList());
                    return Bad("Product belongs to a number of playlist" + list);
                }

                return Bad("Product does not exist ");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again" + ex.Message);
            }
        }
    }
}
