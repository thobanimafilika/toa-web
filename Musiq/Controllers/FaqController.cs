﻿using AutoMapper;
using Musiq.App_Start;
using Musiq.Business.Core;
using Musiq.Business.DB;
using Musiq.Common;
using Musiq.Common.Dto;
using Musiq.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Musiq.Controllers
{
   [RoutePrefix("api/[controller]")] 
    public class FaqController : BaseController
    {

        IUnitOfWork _unitOfWork;
        public FaqController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [HttpGet()]
        public HttpResponseMessage SyncFAQ()
        {
            try
            {

                var result = _unitOfWork.FrequentlyAskedQuestionRepository.GetList(c => !c.IsDeleted);
                if (result != null)
                {
                    return OkResult(result);
                }
                return Bad("no results");

            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpGet()]
        public HttpResponseMessage Search(int pageNumber, int pageSize, string q)
        {
            try
            {
                var result = new PageList<FrequentlyAskedQuestion>(new List<FrequentlyAskedQuestion>(), 1, 1, 10);

                if (!string.IsNullOrEmpty(q))
                {
                    result = _unitOfWork.FrequentlyAskedQuestionRepository.Find(c => c.Question.ToLower().Contains(string.IsNullOrEmpty(q) ? "" : q.ToLower()), new UserParam() { PageNumber = pageNumber, PageSize = pageSize, Search = string.IsNullOrEmpty(q) ? "" : q.ToLower() });

                }
                else
                {
                    result = _unitOfWork.FrequentlyAskedQuestionRepository.Find(c => c.IsDeleted == false, new UserParam() { PageNumber = pageNumber, PageSize = pageSize });
                }

                if (result != null)
                {
                    return OkResult(result.ToPagedMapper<FrequentlyAskedQuestionDto, FrequentlyAskedQuestion>());
                }

                return Bad("No data");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        } 
        
        [HttpGet()]
        public async Task<HttpResponseMessage> GetById(int id)
        {
            try
            {
                var result = await _unitOfWork.FrequentlyAskedQuestionRepository.SingleOrDefault(c => c.FrequentlyAskedQuestionId == id);
                if (result != null)
                {
                    var faq = Mapper.Map<FrequentlyAskedQuestionDto>(result);
                    return OkResult(faq);
                }

                return Bad("faq doesn't exists");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again");
            }
        }

        [HttpPost()]
        public async Task<HttpResponseMessage> Save([System.Web.Http.FromBody]FrequentlyAskedQuestionDto faqDto)
        {
            try
            {
                if (faqDto == null)
                    return Bad("All fieds are required");

                var faq = Mapper.Map<FrequentlyAskedQuestion>(faqDto);

                if (faq.FrequentlyAskedQuestionId ==0)
                {
                    faq.CreatedDate = DateTime.Now;
                }
                else
                {
                    faq.ModifiedDate = DateTime.Now;
                }

                if (string.IsNullOrEmpty(faq.Answer))
                {
                    throw new Exception("Answer is required");
                }

                if (string.IsNullOrEmpty(faq.Question))
                {
                    throw new Exception("Question is required");
                }


                faq.CreatedBy = GetUserId(); 
                
                if (faq.FrequentlyAskedQuestionId <= 0)
                {
                    _unitOfWork.FrequentlyAskedQuestionRepository.Add(faq);
                }
                else
                {
                     await _unitOfWork.FrequentlyAskedQuestionRepository.Update(faq);
                }
                 await _unitOfWork.Complete();
                if (faq?.FrequentlyAskedQuestionId > 0)
                    return OkResult(faq?.FrequentlyAskedQuestionId ?? -1);
                else
                    return Bad("An error occured, please try again");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again "+ ex.Message);
            }
        }

        [HttpGet()]
        public async Task<HttpResponseMessage> Delete(int id = -1)
        {
            try
            {
                var result = await _unitOfWork.FrequentlyAskedQuestionRepository.SingleOrDefault(c => c.FrequentlyAskedQuestionId == id);
                if (result != null)
                {
                    
                        result.IsDeleted = true;
                        await _unitOfWork.FrequentlyAskedQuestionRepository.Update(result);
                        await _unitOfWork.Complete();
                        return OkResult("Deleted");
                }
                return Bad("faq does not exist ");
            }
            catch (Exception ex)
            {
                return Bad("An error occured, please try again" + ex.Message);
            }
        }
    }
}
