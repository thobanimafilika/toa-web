﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Musiq.App_Start
{
    public class Token
    {
        public SecurityToken SecurityToken
        {
            get; set;
        }

        public string Value
        {
            get; set;
        }

        public object GetObject()
        {
            return new
            {
                access_token = Value,
                token_type = "bearer",
                expires_in = SecurityToken.ValidTo.ToString()
            };
        }
    }
}