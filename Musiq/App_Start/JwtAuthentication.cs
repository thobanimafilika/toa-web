﻿using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Web;

namespace Musiq.App_Start
{
    public class JwtAuthentication
    {
        public static Token GenerateToken(string username)
        {
            string Secret = ConfigurationManager.AppSettings["AuthenticationSecretKey"];

            var symmetricKey = Convert.FromBase64String(Secret);
            var tokenHandler = new JwtSecurityTokenHandler();

            var now = DateTime.UtcNow;
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                        {
                        new Claim(ClaimTypes.Name, username)
                    }),

                Expires = now.AddYears(1),

                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
            };

            var stoken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(stoken);


            return new Token()
            {
                SecurityToken = stoken,
                Value = token
            };
        }



        public static string ExtractUserName(string token)
        {
            string username = null;

            var simplePrinciple = GetPrincipal(token);

            ClaimsIdentity identity = null;

            try
            {
                identity = simplePrinciple.Identity as ClaimsIdentity;
            }
            catch (Exception ex)
            {
                return null;
            }
            if (identity == null)
                return null;

            if (!identity.IsAuthenticated)
                return null;

            var usernameClaim = identity.FindFirst(ClaimTypes.Name);
            username = usernameClaim?.Value;

            if (string.IsNullOrEmpty(username))
                return null;

            return username;
        }



        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                token = token.Replace("Bearer ", string.Empty);

                string Secret = ConfigurationManager.AppSettings["AuthenticationSecretKey"];

                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                var symmetricKey = Convert.FromBase64String(Secret);
                var IssuerSigningKey = new SymmetricSecurityKey(symmetricKey);
                IdentityModelEventSource.ShowPII = true;
                var SigningCredentials = new SigningCredentials(
                         IssuerSigningKey,
                         SecurityAlgorithms.HmacSha256Signature);

                var handler = new JwtSecurityTokenHandler();
                var tokenSecure = handler.ReadToken(token) as SecurityToken;
                var validationParameters = new TokenValidationParameters
                {

                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = IssuerSigningKey,
                    ValidateIssuer = false,
                    ValidateAudience = false
                };

                

               /* var validationParameters = new TokenValidationParameters()
                {
                   
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };*/
          

                SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

                return principal;
            }

            catch (Exception ex)
            {
                return null;
            }
        }
    }
}