﻿using System;
using Microsoft.IdentityModel.Tokens;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using System.Threading;
using System.Net.Http.Headers;

namespace Musiq.App_Start
{
    public class JwtAuthorize : Attribute, IAuthenticationFilter
    {
        public bool AllowMultiple
        {
            get; private set;
        }

        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {

            await Task.Factory.StartNew(() =>
            {

                var req = context.Request;

                if (req.Headers.Authorization != null && req.Headers.Authorization.Scheme.Equals("bearer", StringComparison.OrdinalIgnoreCase))
                {
                    var principal = JwtAuthentication.GetPrincipal(req.Headers.Authorization.Parameter);

                    if (principal == null || !principal.Identity.IsAuthenticated)
                        context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);

                    context.Principal = principal;
                }
                else
                {
                    context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[0], context.Request);
                }

            });


        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {

            return Task.FromResult(0);
        }
    }
   }